# Hello Web App

## Setup
* Fork this repo to your account
* Clone the forked repo to local workspace
* Install required platforms:

|Platform|Version|
| ------------------- | ------------------- |
|Visual Studio|2017+|
|Visual Studio Code|
|Asp.Net Core SDK|2.2+|
|MS SQL Server|2016+|

## Visual Studio Code
List of must-have extensions

* Angular 7 Snippets
* Angular Files
* Auto Import

## Database Migrations
To prevent potential conflicts, please notify the team and let 1 person do it and commit at a time only

## Admin App
* Open {workspace}/WebApp/Angular/AdminApp
* Open terminal from this folder
* Execute `npm install` to install npm packages for THE FIRST TIME or for restoring npm packages
* Run "npm start" to start Angular CLI server
* Run Web App in Visual Studio

## Admin Demo
* Open {workspace}/WebApp/Angular/AdminDemo
* Open terminal from this folder
* Execute `npm install` to install npm packages
* Execute `ng serve --open` to run demo

## License
The MIT License (MIT).

## Contribution
Feel free to create issues and PRs 😃