﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class GoodsReceiptRepository : RepositoryBase<GoodsReceipt>, IGoodsReceiptRepository
    {
        public GoodsReceiptRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        protected override async Task LoadChildrenAsync(GoodsReceipt entity)
        {
            await DbContext.Entry(entity).Collection(x => x.Items).LoadAsync();
        }
    }
}
