﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.EmployeeAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeReposiory
    {
        public EmployeeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
