﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class StoreRepository : RepositoryBase<Store>, IStoreRepository
    {
        public StoreRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
