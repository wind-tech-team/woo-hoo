﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class MaterialGroupRepository : RepositoryBase<MaterialGroup>, IMaterialGroupRepository
    {
        public MaterialGroupRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
