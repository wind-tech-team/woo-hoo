﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class InventoryRepository : RepositoryBase<Inventory>, IInventoryRepository
    {
        public InventoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        protected override async Task LoadChildrenAsync(Inventory entity)
        {
            await DbContext.Entry(entity).Collection(x => x.MaterialItems).LoadAsync();
            await DbContext.Entry(entity).Collection(x => x.ProductItems).LoadAsync();
        }

        public async Task<Inventory> GetInventoryByStoreIdAsync(int storeId)
        {
            return await DbContext.Set<Inventory>().FirstOrDefaultAsync(i => i.StoreId == storeId);
        }
    }
}
