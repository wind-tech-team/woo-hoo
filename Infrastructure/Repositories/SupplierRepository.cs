﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.SupplierAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class SupplierRepository : RepositoryBase<Supplier>, ISupplierRepository
    {
        public SupplierRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
