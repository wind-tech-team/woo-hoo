﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class ProductGroupRepository : RepositoryBase<ProductGroup>, IProductGroupRepository
    {
        public ProductGroupRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
