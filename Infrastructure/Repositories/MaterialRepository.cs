﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Infrastructure.Seedwork;

namespace WooHoo.Infrastructure.Repositories
{
    public class MaterialRepository : RepositoryBase<Material>, IMaterialRepository
    {
        public MaterialRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
