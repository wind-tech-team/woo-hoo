﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class MaterialGroupConfiguration : IEntityTypeConfiguration<MaterialGroup>
    {
        public void Configure(EntityTypeBuilder<MaterialGroup> builder)
        {
            // table
            builder.ToTable("MaterialGroups");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("MaterialGroupsSequenceHiLo");

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
