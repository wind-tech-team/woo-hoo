﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
using WooHoo.Domain.Aggregates.MaterialAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class GoodsReceiptItemConfiguration : IEntityTypeConfiguration<GoodsReceiptItem>
    {
        public void Configure(EntityTypeBuilder<GoodsReceiptItem> builder)
        {
            // table
            builder.ToTable("GoodsReceiptItems");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("GoodsReceiptItemsSequenceHiLo");

            // FK
            builder.HasOne<Material>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.MaterialId);

            builder.Property<int>("GoodsReceiptId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
