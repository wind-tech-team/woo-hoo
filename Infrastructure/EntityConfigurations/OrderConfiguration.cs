﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.OrderAggregate;
using WooHoo.Domain.Aggregates.PromotionAggregate;
using WooHoo.Domain.Aggregates.StoreAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            // table
            builder.ToTable("Orders");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("OrdersSequenceHiLo");

            // FK
            builder.HasOne<Store>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.StoreId);

            builder.HasOne<Promotion>()
                .WithMany()
                .HasForeignKey(x => x.PromotionId);

            builder.HasOne(x => x.Type)
                .WithMany()
                .IsRequired()
                .HasForeignKey("TypeId");

            builder.HasOne(x => x.Status)
                .WithMany()
                .IsRequired()
                .HasForeignKey("StatusId");

            // complex type
            builder.OwnsOne(x => x.DeliveryInfo).OwnsOne(x => x.Address);

            // collection
            builder.Metadata.FindNavigation(nameof(Order.Items)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Code).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();

            // computed
            builder.Property<int>("AutoIncrement").UseSqlServerIdentityColumn();
            builder.Property(p => p.Code)
                .HasComputedColumnSql("'DH' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)");
        }
    }
}
