﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WooHoo.Domain.Aggregates.GoodsIssueAggregate;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Aggregates.SupplierAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class GoodsIssueItemConfiguration : IEntityTypeConfiguration<GoodsIssueItem>
    {
        public void Configure(EntityTypeBuilder<GoodsIssueItem> builder)
        {
            // table
            builder.ToTable("GoodsIssueItems");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("GoodsIssueItemsSequenceHiLo");

            // FK
            builder.HasOne<Material>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.MaterialId);

            builder.HasOne<Supplier>()
                .WithMany()
                .HasForeignKey(x => x.SupplierId);

            builder.Property<int>("GoodsIssueId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
