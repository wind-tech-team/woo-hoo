﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.OrderAggregate;
using WooHoo.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            // table
            builder.ToTable("OrderItems");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("OrderItemsSequenceHiLo");

            // FK
            builder.HasOne<Product>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.ProductId);

            builder.Property<int>("OrderId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
