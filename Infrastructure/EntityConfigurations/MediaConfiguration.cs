﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MediaAggregate;
using WooHoo.Domain.Aggregates.ProductAggregate;
using WooHoo.Domain.Aggregates.StoreAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class MediaConfiguration : IEntityTypeConfiguration<Media>
    {
        public void Configure(EntityTypeBuilder<Media> builder)
        {
            // table
            builder.ToTable("Media");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("MediaSequenceHiLo");

            // FK
            builder.HasOne(x => x.Type)
                .WithMany()
                .IsRequired()
                .HasForeignKey("TypeId");

            builder.HasOne<Store>()
                .WithMany()
                .HasForeignKey(x => x.StoreId);

            builder.HasOne<Product>()
                .WithMany()
                .HasForeignKey(x => x.ProductId);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Source).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
