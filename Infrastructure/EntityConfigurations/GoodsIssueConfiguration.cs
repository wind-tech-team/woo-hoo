﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WooHoo.Domain.Aggregates.GoodsIssueAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class GoodsIssueConfiguration : IEntityTypeConfiguration<GoodsIssue>
    {
        public void Configure(EntityTypeBuilder<GoodsIssue> builder)
        {
            // table
            builder.ToTable("GoodsIssues");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("GoodsIssuesSequenceHiLo");

            // collection
            builder.Metadata.FindNavigation(nameof(GoodsIssue.Items)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Code).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();

            // computed
            builder.Property<int>("AutoIncrement").UseSqlServerIdentityColumn();
            builder.Property(p => p.Code)
                .HasComputedColumnSql("'PX' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)");
        }
    }
}
