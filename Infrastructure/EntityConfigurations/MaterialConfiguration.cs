﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class MaterialConfiguration : IEntityTypeConfiguration<Material>
    {
        public void Configure(EntityTypeBuilder<Material> builder)
        {
            // table
            builder.ToTable("Materials");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("MaterialsSequenceHiLo");

            // FK
            builder.HasOne<MaterialGroup>()
                .WithMany()
                .HasForeignKey(x => x.MaterialGroupId);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Code).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.SaleUnit).IsRequired();
            builder.Property(x => x.InventoryUnit).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();

            // computed
            builder.Property<int>("AutoIncrement").UseSqlServerIdentityColumn();
            builder.Property(p => p.Code)
                .HasComputedColumnSql("'NL' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)");
        }
    }
}
