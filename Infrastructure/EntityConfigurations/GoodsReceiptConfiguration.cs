﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class GoodsReceiptConfiguration : IEntityTypeConfiguration<GoodsReceipt>
    {
        public void Configure(EntityTypeBuilder<GoodsReceipt> builder)
        {
            // table
            builder.ToTable("GoodsReceipts");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("GoodsReceiptsSequenceHiLo");

            // collection
            builder.Metadata.FindNavigation(nameof(GoodsReceipt.Items)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Code).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();

            // computed
            builder.Property<int>("AutoIncrement").UseSqlServerIdentityColumn();
            builder.Property(p => p.Code)
                .HasComputedColumnSql("'PN' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)");
        }
    }
}
