﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class ProductComponentConfiguration : IEntityTypeConfiguration<ProductComponent>
    {
        public void Configure(EntityTypeBuilder<ProductComponent> builder)
        {
            // table
            builder.ToTable("ProductComponents");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("ProductComponentsSequenceHiLo");

            // FK
            builder.HasOne<Material>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.MaterialId);

            builder.Property<int>("ProductId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
