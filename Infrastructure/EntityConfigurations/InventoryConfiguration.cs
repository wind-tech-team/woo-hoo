﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Aggregates.StoreAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class InventoryConfiguration : IEntityTypeConfiguration<Inventory>
    {
        public void Configure(EntityTypeBuilder<Inventory> builder)
        {
            // table
            builder.ToTable("Inventories");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("InventoriesSequenceHiLo");

            // FK
            builder.HasOne<Store>()
                .WithMany()
                .HasForeignKey(x => x.StoreId);

            // collection
            builder.Metadata.FindNavigation(nameof(Inventory.MaterialItems)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.Metadata.FindNavigation(nameof(Inventory.ProductItems)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
