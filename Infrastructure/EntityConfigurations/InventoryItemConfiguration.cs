﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WooHoo.Domain.Aggregates.ProductAggregate;
using WooHoo.Domain.Aggregates.SupplierAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class MaterialInventoryItemConfiguration : IEntityTypeConfiguration<MaterialInventoryItem>
    {
        public void Configure(EntityTypeBuilder<MaterialInventoryItem> builder)
        {
            // table
            builder.ToTable("MaterialInventoryItems");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("MaterialInventoryItemsSequenceHiLo");

            // FK
            builder.HasOne<Material>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.MaterialId);

            builder.HasOne<Supplier>()
                .WithMany()
                .HasForeignKey(x => x.SupplierId);

            builder.Property<int>("InventoryId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }

    class ProductInventoryItemConfiguration : IEntityTypeConfiguration<ProductInventoryItem>
    {
        public void Configure(EntityTypeBuilder<ProductInventoryItem> builder)
        {
            // table
            builder.ToTable("ProductInventoryItems");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("ProductInventoryItemsSequenceHiLo");

            // FK
            builder.HasOne<Product>()
                .WithMany()
                .IsRequired()
                .HasForeignKey(x => x.ProductId);

            builder.HasOne<Supplier>()
                .WithMany()
                .HasForeignKey(x => x.SupplierId);

            builder.Property<int>("InventoryId")
                .IsRequired();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
