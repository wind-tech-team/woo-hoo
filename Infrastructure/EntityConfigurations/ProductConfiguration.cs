﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.ProductAggregate;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            // table
            builder.ToTable("Products");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("ProductsSequenceHiLo");

            // FK
            builder.HasOne<ProductGroup>()
                .WithMany()
                .HasForeignKey(x => x.ProductGroupId);

            // collection
            builder.Metadata.FindNavigation(nameof(Product.Components)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Code).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();

            // computed
            builder.Property<int>("AutoIncrement").UseSqlServerIdentityColumn();
            builder.Property(p => p.Code)
                .HasComputedColumnSql("'SP' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)");
        }
    }
}
