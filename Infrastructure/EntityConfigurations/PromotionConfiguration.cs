﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.PromotionAggregate;
using WooHoo.Domain.Aggregates.StoreAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class PromotionConfiguration : IEntityTypeConfiguration<Promotion>
    {
        public void Configure(EntityTypeBuilder<Promotion> builder)
        {
            // table
            builder.ToTable("Promotions");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("PromotionsSequenceHiLo");

            // FK
            builder.HasOne<Store>()
                .WithMany()
                .HasForeignKey(x => x.StoreId);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
