﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Aggregates.EmployeeAggregate;

namespace WooHoo.Infrastructure.EntityConfigurations
{
    class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("EmployeesSequenceHilo");

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
