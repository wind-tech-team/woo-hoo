﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WooHoo.Common.Helpers;
using WooHoo.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;

namespace WooHoo.Infrastructure.Seedwork
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : AggregateRoot
    {
        protected readonly ApplicationDbContext DbContext;

        public IUnitOfWork UnitOfWork => DbContext;

        protected RepositoryBase(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected virtual Task LoadChildrenAsync(T entity)
        {
            return Task.CompletedTask;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var entity = await DbContext.Set<T>().FindAsync(id);
            if (entity != null)
            {
                await LoadChildrenAsync(entity);
            }

            return entity;
        }

        public async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> filter)
        {
            var entities = await DbContext.Set<T>().Where(filter).ToListAsync();
            foreach(var entity in entities)
            {
                await LoadChildrenAsync(entity);
            }
            return entities;
        }

        public Task InsertAsync(T entity)
        {
            return DbContext.Set<T>().AddAsync(entity);
        }

        public Task InsertRangeAsync(IEnumerable<T> entities)
        {
            return DbContext.Set<T>().AddRangeAsync(entities);
        }

        public void Update(T entity, string currentRowVersion)
        {
            if (string.IsNullOrWhiteSpace(currentRowVersion))
                throw new ArgumentNullException(nameof(currentRowVersion));

            entity.RowVersion = ByteArrayConverter.FromString(currentRowVersion);
            DbContext.Set<T>().Update(entity);
        }

        public void Update(T entity, byte[] currentRowVersion)
        {
            entity.RowVersion = currentRowVersion ?? throw new ArgumentNullException(nameof(currentRowVersion));
            DbContext.Set<T>().Update(entity);
        }

        public void Delete(T entity, string currentRowVersion)
        {
            if (string.IsNullOrWhiteSpace(currentRowVersion))
                throw new ArgumentNullException(nameof(currentRowVersion));

            entity.RowVersion = ByteArrayConverter.FromString(currentRowVersion);
            DbContext.Set<T>().Remove(entity);
        }
    }
}
