﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WooHoo.Infrastructure
{
    public sealed class ApplicationDbContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;
        private readonly ServiceContext _serviceContext;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IMediator mediator, ServiceContext serviceContext) : base(options)
        {
            _mediator = mediator;
            _serviceContext = serviceContext;

            //ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            ConfigureEntities(builder);
            ConfigureSoftDelete(builder);
        }

        public async Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);

            bool saveFailed;

            switch (strategy)
            {
                case ConcurrencyResolutionStrategy.None:
                    try
                    {
                        // https://stackoverflow.com/questions/4402586/optimisticconcurrencyexception-does-not-work-in-entity-framework-in-certain-situ
                        var isRowVersionChanged = ChangeTracker.Entries()
                            .Any(x => x.Properties.Any(m => m.Metadata.Name == "RowVersion") && x.CurrentValues.GetValue<byte[]>("RowVersion") != null && !x.CurrentValues.GetValue<byte[]>("RowVersion").SequenceEqual(x.OriginalValues.GetValue<byte[]>("RowVersion")));
                        if (isRowVersionChanged)
                        {
                            throw new DomainException("Concurrency") { IsDbConcurrencyUpdate = true };
                        }

                        PreSaveChanges();

                        await SaveChangesAsync(cancellationToken);
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        throw new DomainException(ex.Message) { IsDbConcurrencyUpdate = true };
                    }

                    break;
                case ConcurrencyResolutionStrategy.DatabaseWin:
                    do
                    {
                        saveFailed = false;

                        try
                        {
                            PreSaveChanges();

                            await SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update the values of the Entity that failed to save from the store 
                            ex.Entries.Single().Reload();
                        }

                    } while (saveFailed);

                    break;
                case ConcurrencyResolutionStrategy.ClientWin:
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            PreSaveChanges();

                            await SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update original values from the database 
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }

                    } while (saveFailed);


                    break;
            }
        }

        public void BeginTransaction()
        {
            throw new NotImplementedException();
        }

        public void CommitTransaction()
        {
            throw new NotImplementedException();
        }

        public void RollbackTransaction()
        {
            throw new NotImplementedException();
        }

        private void PreSaveChanges()
        {
            HandleSoftDelete();
            HandleAudit();
        }

        private void HandleSoftDelete()
        {
            foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted))
            {
                if (entry.Entity is ISoftDeletable)
                {
                    entry.Property("IsDeleted").CurrentValue = true;
                    entry.State = EntityState.Modified;
                }
            }
        }

        private void HandleAudit()
        {
            foreach(var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (entry.Entity is IAuditable auditable)
                {
                    if (entry.State == EntityState.Added)
                    {
                        auditable.CreatedBy = _serviceContext.Principal.Username;
                        auditable.CreatedDate = DateTime.Now;
                    }
                    else
                    {
                        auditable.UpdatedBy = _serviceContext.Principal.Username;
                        auditable.UpdatedDate = DateTime.Now;
                    }
                }
            }
        }

        private void ConfigureEntities(ModelBuilder builder)
        {
            var types = typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetTypes();
            var maps = (from t in types
                        where t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))
                              && !t.GetTypeInfo().IsAbstract
                              && !t.GetTypeInfo().IsInterface
                        select Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                /*
                 a bit hack here which just gets the first ApplyConfiguration method
                 there are several overloads of this method => find a way to get the "correct" one instead of the "first" one
                 */
                var methodInfo = typeof(ModelBuilder).GetMethods().First(x => x.Name == nameof(builder.ApplyConfiguration));
                methodInfo = methodInfo.MakeGenericMethod(map.GetType().GetInterfaces().First().GenericTypeArguments);
                methodInfo.Invoke(builder, new[] { map });
            }
        }

        private void ConfigureSoftDelete(ModelBuilder builder)
        {
            foreach(var entity in builder.Model.GetEntityTypes())
            {
                if(typeof(ISoftDeletable).IsAssignableFrom(entity.ClrType) && entity.BaseType == null)
                {
                    builder
                        .Entity(entity.ClrType)
                        .HasDiscriminator("IsDeleted", typeof(bool))
                        .HasValue(false);

                    builder
                        .Entity(entity.ClrType)
                        .Property(typeof(bool), "IsDeleted")
                        .IsRequired()
                        .HasDefaultValue(false);

                    builder
                        .Entity(entity.ClrType)
                        .Property(typeof(bool), "IsDeleted")
                        .Metadata
                        .AfterSaveBehavior = PropertySaveBehavior.Save;
                }
            }
        }
    }
}
