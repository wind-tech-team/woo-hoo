﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WooHoo.WebApp.Infrastructure.JsonConverters
{
    public class LocalDateTimeConverter : IsoDateTimeConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var utcDate = DateTime.Parse(reader.Value.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind);
            var localDate = TimeZoneInfo.ConvertTimeFromUtc(utcDate, TimeZoneInfo.Local);

            return localDate;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            base.WriteJson(writer, value, serializer);
        }
    }
}
