﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace WooHoo.WebApp.Infrastructure.Conventions
{
    public class AddAuthorizeFilterConvention : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            if (controller.Attributes.Any(x => x is ApiControllerAttribute))
            {
                controller.Filters.Add(new AuthorizeFilter("Api"));
            }
            else
            {
                controller.Filters.Add(new AuthorizeFilter("Default"));
            }
        }
    }
}
