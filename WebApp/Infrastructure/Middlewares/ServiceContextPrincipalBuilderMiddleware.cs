﻿using System.Security.Claims;
using System.Threading.Tasks;
using WooHoo.Domain.Seedwork;
using Microsoft.AspNetCore.Http;

namespace WooHoo.WebApp.Infrastructure.Middlewares
{
    public class ServiceContextPrincipalBuilderMiddleware
    {
        private readonly RequestDelegate _next;

        public ServiceContextPrincipalBuilderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var ctx = (ServiceContext)context.RequestServices.GetService(typeof(ServiceContext));

            if (context.User.Identity.IsAuthenticated)
            {
                foreach (var claim in context.User.Claims)
                {
                    switch (claim.Type)
                    {
                        case ClaimTypes.Name:
                            ctx.Principal.Username = claim.Value;
                            break;
                        case ClaimTypes.NameIdentifier:
                            ctx.Principal.UserId = claim.Value;
                            break;
                        case ClaimTypes.Role:
                            ctx.Principal.Role = claim.Value;
                            break;
                    }
                }
            }
            else
            {
                ctx.Principal.Username = "anonymous";
            }

            await _next.Invoke(context);
        }
    }
}
