﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WooHoo.WebApp.Infrastructure.Migrations
{
    public partial class Update_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "GoodsIssueItemsSequenceHiLo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "GoodsIssuesSequenceHiLo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "GoodsIssues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    IssueDate = table.Column<DateTime>(nullable: false),
                    InventoryId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: false, computedColumnSql: "'PX' + FORMAT(GETDATE(), 'yy') + RIGHT(CONCAT('000000', ISNULL([AutoIncrement],'')), 6)"),
                    Note = table.Column<string>(nullable: true),
                    AutoIncrement = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsIssues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GoodsIssueItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    MaterialId = table.Column<int>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    SupplierId = table.Column<int>(nullable: true),
                    ConsignmentCode = table.Column<string>(nullable: true),
                    ReceiptDate = table.Column<DateTime>(nullable: false),
                    GoodsIssueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsIssueItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoodsIssueItems_GoodsIssues_GoodsIssueId",
                        column: x => x.GoodsIssueId,
                        principalTable: "GoodsIssues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoodsIssueItems_Materials_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoodsIssueItems_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GoodsIssueItems_GoodsIssueId",
                table: "GoodsIssueItems",
                column: "GoodsIssueId");

            migrationBuilder.CreateIndex(
                name: "IX_GoodsIssueItems_MaterialId",
                table: "GoodsIssueItems",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_GoodsIssueItems_SupplierId",
                table: "GoodsIssueItems",
                column: "SupplierId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoodsIssueItems");

            migrationBuilder.DropTable(
                name: "GoodsIssues");

            migrationBuilder.DropSequence(
                name: "GoodsIssueItemsSequenceHiLo");

            migrationBuilder.DropSequence(
                name: "GoodsIssuesSequenceHiLo");
        }
    }
}
