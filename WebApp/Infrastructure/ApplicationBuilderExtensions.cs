﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;

namespace WooHoo.WebApp.Infrastructure
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder RequireAuthenticationOn(this IApplicationBuilder app, string pathPrefix)
        {
            return app.Use((context, next) =>
            {
                if (context.Request.Path.HasValue && context.Request.Path.Value.StartsWith(pathPrefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!context.User.Identity.IsAuthenticated)
                    {
                        return context.ChallengeAsync();
                    }
                }

                return next();
            });
        }
    }
}
