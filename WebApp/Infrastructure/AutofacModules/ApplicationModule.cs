﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Domain.Seedwork;
using WooHoo.Infrastructure.Repositories;
using WooHoo.WebApp.Application.Queries;

namespace WooHoo.WebApp.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StoreQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<MaterialGroupQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<MaterialQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProductGroupQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<SupplierQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<EmployeeQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(StoreRepository).GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(InventoryRepository).GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ServiceContext>().AsSelf().InstancePerLifetimeScope();

            builder.RegisterType<InventoryQueries>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

        }
    }
}
