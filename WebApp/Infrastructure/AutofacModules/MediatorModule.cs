﻿using Autofac;
using FluentValidation;
using MediatR;
using System.Reflection;
using WooHoo.WebApp.Application.Behaviors;
using WooHoo.WebApp.Application.Commands.ProductGroupCommands;
using WooHoo.WebApp.Application.Commands.StoreCommands;
using WooHoo.WebApp.Application.DomainEventHandlers;
using WooHoo.WebApp.Application.Validators;

namespace WooHoo.WebApp.Infrastructure.AutofacModules
{
    public class MediatorModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register Mediator itself
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            // Register request and notification handlers for Command/CommandHandler and DomainEvent/DomainEventHandler
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // finally register our custom code (individually, or via assembly scanning)
            // - requests & handlers as transient, i.e. InstancePerDependency()
            // - pre/post-processors as scoped/per-request, i.e. InstancePerLifetimeScope()
            // - behaviors as transient, i.e. InstancePerDependency()

            // Register all the CommandHandlers
            builder.RegisterAssemblyTypes(typeof(CreateStoreCommandHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .InstancePerDependency();

            // Register all the Command's Validators
            builder.RegisterAssemblyTypes(typeof(CreateStoreCommandValidator).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .InstancePerDependency();

            // Register the DomainEventHandler classes (they implement INotificationHandler<>) in assembly holding the Domain Events
            builder.RegisterAssemblyTypes(typeof(TestEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .InstancePerDependency();

            // Register behaviors
            builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>)).InstancePerDependency();
        }
    }
}
