﻿using Hangfire.Dashboard;

namespace WooHoo.WebApp.Infrastructure.Filters
{
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpcontext = context.GetHttpContext();
            return httpcontext.User.Identity.IsAuthenticated && httpcontext.User.IsInRole("Administrator");
        }
    }
}
