﻿using System.Net;
using FluentValidation;
using WooHoo.Domain.Exceptions;
using WooHoo.WebApp.Infrastructure.ActionResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace WooHoo.WebApp.Infrastructure.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment _env;
        private readonly ILogger<GlobalExceptionFilter> _logger;

        public GlobalExceptionFilter(IHostingEnvironment env, ILogger<GlobalExceptionFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            if (context.Exception.GetType() == typeof(DomainException) || context.Exception.GetType() == typeof(ValidationException))
            {
                var json = new JsonErrorResponse
                {
                    ErrorMessage = context.Exception.Message
                };

                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                context.ExceptionHandled = true;
            }
            else
            {
                var json = new JsonErrorResponse
                {
                    ErrorMessage = "An error occur.Try it again."
                };

                if (_env.IsDevelopment())
                {
                    json.DeveloperMessage = context.Exception;
                }

                context.Result = new InternalServerErrorObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
        }

        private class JsonErrorResponse
        {
            public string ErrorMessage { get; set; }

            public object DeveloperMessage { get; set; }
        }
    }
}
