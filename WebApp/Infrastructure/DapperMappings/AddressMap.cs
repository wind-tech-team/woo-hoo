﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.FluentMap.Mapping;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Infrastructure.DapperMappings
{
    public class AddressMap : EntityMap<AddressReturnModel>
    {
        public AddressMap()
        {
            Map(p => p.Ward).ToColumn("Address_Ward");
            Map(p => p.City).ToColumn("Address_City");
            Map(p => p.Country).ToColumn("Address_Country");
            Map(p => p.District).ToColumn("Address_District");
            Map(p => p.Street).ToColumn("Address_Street");
            Map(p => p.State).ToColumn("Address_State");
            Map(p => p.ZipCode).ToColumn("Address_ZipCode");
        }
    }
}
