﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.FluentMap;

namespace WooHoo.WebApp.Infrastructure.DapperMappings
{
    public class DapperMapping
    {
        public static void Register()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new AddressMap());
            });
        }
    }
}
