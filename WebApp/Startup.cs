﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using WooHoo.Infrastructure;
using WooHoo.WebApp.Identity;
using WooHoo.WebApp.Infrastructure.AutofacModules;
using WooHoo.WebApp.Infrastructure.Filters;
using WooHoo.WebApp.Infrastructure.Middlewares;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using WooHoo.WebApp.Infrastructure;
using WooHoo.WebApp.Infrastructure.Conventions;
using WooHoo.WebApp.Infrastructure.DapperMappings;
using WooHoo.WebApp.Infrastructure.JsonConverters;
using WooHoo.WebApp.Infrastructure.ModelBinders;

namespace WooHoo.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // AppSettings
            services.AddOptions();

            // EF DB Context
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                        sqlOptions =>
                        {
                            sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                            sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                        });
                });

            // Identity DB Context
            services.AddDbContext<AppIdentityDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Cookie Authentication Scheme
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<AppIdentityDbContext>();
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Auth";
            });

            // JWT Authentication Scheme
            services.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;

                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = "Woohoo_Iss",
                        ValidAudience = "Woohoo_Aud",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Woohoo_Secret_Key"))
                    };

                });

            /*
             * Authorization Policy
             * Default as Cookie Authentication
             * For API, use JWT or fallback to Cookie Authentication for browser logged-in
             */
            services.AddAuthorization(o =>
            {
                o.AddPolicy("Default", b =>
                {
                    b.RequireAuthenticatedUser();
                    b.AuthenticationSchemes = new List<string> { IdentityConstants.ApplicationScheme };
                });
                o.AddPolicy("Api", b =>
                {
                    b.RequireAuthenticatedUser();
                    b.AuthenticationSchemes = new List<string> { JwtBearerDefaults.AuthenticationScheme, IdentityConstants.ApplicationScheme };
                });
            });

            // Hangfire
            services.AddHangfire(config => config.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));

            // MVC
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(GlobalExceptionFilter));
                    options.Conventions.Add(new AddAuthorizeFilterConvention());
                })
                .AddControllersAsServices()
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    //x.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    x.SerializerSettings.Converters.Add(new LocalDateTimeConverter());
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Swagger
            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(x => x.FullName);
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "WooHoo HTTP API",
                    Version = "v1",
                    Description = "The WooHoo HTTP API",
                    TermsOfService = "Terms Of Service"
                });
            });

            //Auto Mapper
            services.AddAutoMapper();

            // Dapper Mapping
            DapperMapping.Register();

            //SignalR
            services.AddSignalR();
            services.AddSignalRCore();

            // Autofac
            var container = new ContainerBuilder();
            container.Populate(services);

            container.RegisterModule(new MediatorModule());
            container.RegisterModule(new ApplicationModule());

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // Authentication
            app.UseAuthentication();

            // Hangfire
            app.UseHangfireServer();
            app.UseHangfireDashboard(/*options: new DashboardOptions { Authorization = new[] { new HangfireAuthorizationFilter() } }*/);

            // ServiceContext
            app.UseMiddleware<ServiceContextPrincipalBuilderMiddleware>();

            // JWT generation
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("Woohoo_Secret_Key"));
            var options = new TokenProviderOptions
            {
                Audience = "Woohoo_Aud",
                Issuer = "Woohoo_Iss",
                Expiration = TimeSpan.FromDays(10),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
            };

            app.UseMiddleware<TokenProviderMiddleware>(Options.Create(options));

            //SignalR
            app.UseSignalR(routes =>
            {
                //routes.MapHub<TrackingHub>("signalr");
            });

            // MVC
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.RequireAuthenticationOn("/admin");
            app.RequireAuthenticationOn("/swagger");

            app.UseMvc(routes =>
            {
                routes.MapRoute("admin-app", "admin/{*catchall}", new { area = "Admin", controller = "App", action = "Index" });

                routes.MapRoute("auth-app", "auth/{*catchall}", new { area = "Auth", controller = "App", action = "Index" });

                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Swagger
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "WooHoo API V1");
                });
        }
    }
}
