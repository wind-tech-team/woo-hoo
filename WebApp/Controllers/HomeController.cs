﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.OrderAggregate;
using WooHoo.Domain.Aggregates.StoreAggregate;
using Microsoft.AspNetCore.Mvc;
using WooHoo.WebApp.Models;

namespace WooHoo.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Test([FromBody]Test test)
        {
            return Ok();
        }
    }

    public class Test
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public Test(string name)
        {
            Id = 2;
            Name = name;
        }
    }
}
