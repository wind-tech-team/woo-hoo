﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WooHoo.WebApp.Areas.Auth.Controllers
{
    [Area("Auth")]
    [AllowAnonymous]
    public class AppController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
