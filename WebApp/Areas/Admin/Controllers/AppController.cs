﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WooHoo.WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AppController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
