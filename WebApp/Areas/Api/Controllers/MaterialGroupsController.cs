﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.MaterialGroupCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MaterialGroupsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMaterialGroupQueries _materialGroupQueries;

        public MaterialGroupsController(IMaterialGroupQueries materialGroupQueries, IMediator mediator)
        {
            this._materialGroupQueries = materialGroupQueries;
            this._mediator = mediator;

        }

        [HttpGet]
        public async Task<ActionResult<PagedList<MaterialGroupReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var materialGroups = await _materialGroupQueries.GetMaterialGroupsAsync(rq);
            return materialGroups;
        }

        [HttpGet("DropdownList")]
        public async Task<ActionResult<List<MaterialGroupReturnModel>>> DropdownList()
        {
            var materialGroups = await _materialGroupQueries.GetMaterialGroupsDropdownListAsync();
            return materialGroups.ToList();
        }

        [HttpPost]
        public async Task<ActionResult<MaterialGroupReturnModel>> Create([FromBody] CreateMaterialGroupCommand command)
        {
            var materialGroupId = await this._mediator.Send(command);
            var materialGroup = await this._materialGroupQueries.GetMaterialGroupAsync(materialGroupId);
            return materialGroup;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<MaterialGroupReturnModel>> Get(int id)
        {
            var materialGroup = await _materialGroupQueries.GetMaterialGroupAsync(id);
            if (materialGroup == null)
                return NotFound();

            return materialGroup;
        }

        [HttpPut]
        public async Task<ActionResult<MaterialGroupReturnModel>> Update([FromBody]UpdateMaterialGroupCommand command)
        {
            var materialGroupId = await _mediator.Send(command);
            var materialGroup = await _materialGroupQueries.GetMaterialGroupAsync(materialGroupId);
            return materialGroup;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteMaterialGroupCommand(id, rowVersionString);

            await _mediator.Send(command);
            return Ok();
        }
    }
}
