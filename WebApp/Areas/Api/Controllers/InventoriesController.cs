﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Helpers;
using WooHoo.WebApp.Application.Commands.InventoryCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InventoriesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IInventoryQueries _inventoryQueries;

        public InventoriesController(IMediator mediator, IInventoryQueries inventoryQueries)
        {
            _mediator = mediator;
            _inventoryQueries = inventoryQueries;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<InventoryReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var inventories = await _inventoryQueries.GetInventoriesAsync(rq);
            return inventories;
        }

        [HttpGet("DropdownList")]
        public async Task<ActionResult<List<DropDownModel>>> DropdownList()
        {
            var inventories = await _inventoryQueries.GetInventoryDropdownListAsync();
            return inventories;
        }

        [HttpPost]
        public async Task<ActionResult<InventoryReturnModel>> Create([FromBody]CreateInventoryCommand command)
        {
            var inventoryId = await _mediator.Send(command);
            var inventory = await _inventoryQueries.GetInventoryAsync(inventoryId);
            return inventory;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<InventoryReturnModel>> Get(int id)
        {
            var inventory = await _inventoryQueries.GetInventoryAsync(id);
            if (inventory == null)
            {
                return NotFound();
            }

            return inventory;
        }

        [HttpPut]
        public async Task<ActionResult<InventoryReturnModel>> Update([FromBody]UpdateInventoryCommand command)
        {
            var inventoryId = await _mediator.Send(command);
            var inventory = await _inventoryQueries.GetInventoryAsync(inventoryId);
            return inventory;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteInventoryCommand(id, rowVersionString);
            await _mediator.Send(command);
            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody]DeleteInventoriesCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }

        [HttpPost]
        [Route("InputMaterials")]
        public async Task<ActionResult<string>> InputMaterials([FromBody]InputMaterialCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }

        [HttpGet]
        [Route("{id:int}/MaterialItems")]
        public async Task<ActionResult<List<MaterialInventoryItemReturnModel>>> GetMaterialItems(int id)
        {
            var items = await _inventoryQueries.GetMaterialItems(id);
            return items;
        }
    }
}
