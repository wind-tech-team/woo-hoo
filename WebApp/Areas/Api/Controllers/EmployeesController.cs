﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.EmployeeCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IEmployeeQueries _employeeQueries;

        public EmployeesController(IEmployeeQueries employeeQueries, IMediator mediator)
        {
             _employeeQueries = employeeQueries;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<EmployeeReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var employees = await _employeeQueries.GetEmployeesAsync(rq);
            return employees;
        }

        [HttpPost]
        public async Task<ActionResult<EmployeeReturnModel>> Create([FromBody] CreateEmployeeCommand command)
        {
            var employeeId = await _mediator.Send(command);
            var employee = await _employeeQueries.GetEmployeeAsync(employeeId);
            return employee;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<EmployeeReturnModel>> Get(int id)
        {
            var employee = await _employeeQueries.GetEmployeeAsync(id);
            if (employee == null) return NotFound();

            return employee;
        }

        [HttpPut]
        public async Task<ActionResult<EmployeeReturnModel>> Update([FromBody] UpdateEmployeeCommand command)
        {
            var employeeId = await _mediator.Send(command);
            var employee = await _employeeQueries.GetEmployeeAsync(employeeId);
            return employee;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteEmployeeCommand(id, rowVersionString);

            await _mediator.Send(command);
            return Ok();
        }
    }
}
