﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.MaterialCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MaterialsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMaterialQueries _materialQueries;

        public MaterialsController(IMediator mediator, IMaterialQueries materialQueries)
        {
            _mediator = mediator;
            _materialQueries = materialQueries;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<MaterialReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var materials = await _materialQueries.GetMaterialsAsync(rq);
            return materials;
        }

        [HttpGet("DropdownList")]
        public async Task<ActionResult<List<DropDownModel>>> DropdownList()
        {
            var materials = await _materialQueries.GetMaterialDropdownListAsync();
            return materials;
        }

        [HttpPost]
        public async Task<ActionResult<MaterialReturnModel>> Create([FromBody]CreateMaterialCommand command)
        {
            var materialId = await _mediator.Send(command);
            var material = await _materialQueries.GetMaterialAsync(materialId);
            return material;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<MaterialReturnModel>> Get(int id)
        {
            var material = await _materialQueries.GetMaterialAsync(id);
            if (material == null)
            {
                return NotFound();
            }

            return material;
        }

        [HttpPut]
        public async Task<ActionResult<MaterialReturnModel>> Update([FromBody]UpdateMaterialCommand command)
        {
            var materialId = await _mediator.Send(command);
            var material = await _materialQueries.GetMaterialAsync(materialId);
            return material;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteMaterialCommand(id, rowVersionString);
            await _mediator.Send(command);
            return Ok();
        }
    }
}
