﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.ProductGroupCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductGroupsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProductGroupQueries _productGroupQueries;

        public ProductGroupsController(IProductGroupQueries productGroupQueries, IMediator mediator)
        {
            _productGroupQueries = productGroupQueries;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<ProductGroupReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var productGroups = await _productGroupQueries.GetProductGroupsAsync(rq);
            return productGroups;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<ProductGroupReturnModel>> Get(int id)
        {
            var productGroup = await _productGroupQueries.GetProductGroupAsync(id);
            if (productGroup == null)
                return NotFound();

            return productGroup;
        }

        [HttpPost]
        public async Task<ActionResult<ProductGroupReturnModel>> Create([FromBody]CreateProductGroupCommand command)
        {
            var productGroupId = await _mediator.Send(command);
            var productGroup = await _productGroupQueries.GetProductGroupAsync(productGroupId);
            return productGroup;
        }

        [HttpPut]
        public async Task<ActionResult<ProductGroupReturnModel>> Update([FromBody]UpdateProductGroupCommand command)
        {
            var productGroupId = await _mediator.Send(command);
            var productGroup = await _productGroupQueries.GetProductGroupAsync(productGroupId);
            return productGroup;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteProductGroupCommand(id, rowVersionString);

            await _mediator.Send(command);
            return Ok();
        }
    }
}
