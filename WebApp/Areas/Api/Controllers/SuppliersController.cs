﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.SupplierCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SuppliersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ISupplierQueries _supplierQueries;

        public SuppliersController(ISupplierQueries supplierQueries, IMediator mediator)
        {
            _supplierQueries = supplierQueries;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<SupplierReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var suppliers = await _supplierQueries.GetSuppliersAsync(rq);
            return suppliers;
        }

        [HttpGet("DropdownList")]
        public async Task<ActionResult<List<DropDownModel>>> DropdownList()
        {
            var suppliers = await _supplierQueries.GetSupplierDropdownListAsync();
            return suppliers;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<SupplierReturnModel>> Get(int id)
        {
            var supplier = await _supplierQueries.GetSupplierAsync(id);
            if (supplier == null)
                return NotFound();

            return supplier;
        }

        [HttpPost]
        public async Task<ActionResult<SupplierReturnModel>> Create([FromBody]CreateSupplierCommand command)
        {
            var supplierId = await _mediator.Send(command);
            var supplier = await _supplierQueries.GetSupplierAsync(supplierId);
            return supplier;
        }

        [HttpPut]
        public async Task<ActionResult<SupplierReturnModel>> Update([FromBody]UpdateSupplierCommand command)
        {
            var supplierId = await _mediator.Send(command);
            var supplier = await _supplierQueries.GetSupplierAsync(supplierId);
            return supplier;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteSupplierCommand(id, rowVersionString);

            await _mediator.Send(command);
            return Ok();
        }
    }
}
