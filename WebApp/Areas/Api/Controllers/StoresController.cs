﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.StoreCommands;
using WooHoo.WebApp.Application.Queries;
using WooHoo.WebApp.Application.Queries.RequestModels;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Areas.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StoresController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IStoreQueries _storeQueries;

        public StoresController(IStoreQueries storeQueries, IMediator mediator)
        {
            _storeQueries = storeQueries;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<PagedList<StoreReturnModel>>> List([FromQuery]ListRequestModel rq)
        {
            var stores = await _storeQueries.GetStoresAsync(rq);
            return stores;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<StoreReturnModel>> Get(int id)
        {
            var store = await _storeQueries.GetStoreAsync(id);
            if (store == null)
                return NotFound();

            return store;
        }

        [HttpPost]
        public async Task<ActionResult<StoreReturnModel>> Create([FromBody]CreateStoreCommand command)
        {
            var storeId = await _mediator.Send(command);
            var store = await _storeQueries.GetStoreAsync(storeId);
            return store;
        }

        [HttpPut]
        public async Task<ActionResult<StoreReturnModel>> Update([FromBody]UpdateStoreCommand command)
        {
            var storeId = await _mediator.Send(command);
            var store = await _storeQueries.GetStoreAsync(storeId);
            return store;
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id, string rowVersionString)
        {
            var command = new DeleteStoreCommand(id, rowVersionString);

            await _mediator.Send(command);
            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody]DeleteStoresCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }
    }
}
