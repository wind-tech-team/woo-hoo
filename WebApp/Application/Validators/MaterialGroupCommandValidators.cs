﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.MaterialGroupCommands;

namespace WooHoo.WebApp.Application.Validators
{
    public class CreateMaterialGroupCommandValidator : AbstractValidator<CreateMaterialGroupCommand>
    {
        public CreateMaterialGroupCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }

    public class UpdateMaterialGroupCommandValidator : AbstractValidator<UpdateMaterialGroupCommand>
    {
        public UpdateMaterialGroupCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.RowVersionString).NotEmpty();
        }
    }

    public class DeleteMaterialGroupCommandValidator : AbstractValidator<DeleteMaterialGroupCommand>
    {
        public DeleteMaterialGroupCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}

