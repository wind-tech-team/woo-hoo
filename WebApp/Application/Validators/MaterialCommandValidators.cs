﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.MaterialCommands;

namespace WooHoo.WebApp.Application.Validators
{
    public class MaterialCommandValidators : AbstractValidator<CreateMaterialCommand>
    {
        public MaterialCommandValidators()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.InventoryUnit).NotEmpty();
            RuleFor(x => x.SaleUnit).NotEmpty();
            RuleFor(x => x.InventoryToSaleAmountPerUnit).GreaterThan(0);
            RuleFor(x => x.AveragePricePerInventoryUnit).GreaterThan(0);
        }
    }

    public class UpdateMaterialCommandValidator : AbstractValidator<UpdateMaterialCommand>
    {
        public UpdateMaterialCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.InventoryUnit).NotEmpty();
            RuleFor(x => x.SaleUnit).NotEmpty();
            RuleFor(x => x.InventoryToSaleAmountPerUnit).GreaterThan(0);
            RuleFor(x => x.AveragePricePerInventoryUnit).GreaterThan(0);
        }
    }

    public class DeleteMaterialCommandValidator : AbstractValidator<DeleteMaterialCommand>
    {
        public DeleteMaterialCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}

