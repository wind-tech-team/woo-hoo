﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using WooHoo.WebApp.Application.Commands.ProductGroupCommands;

namespace WooHoo.WebApp.Application.Validators
{
    public class CreateProductGroupCommandValidator : AbstractValidator<CreateProductGroupCommand>
    {
        public CreateProductGroupCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }

    public class UpdateProductGroupCommandValidator : AbstractValidator<UpdateProductGroupCommand>
    {
        public UpdateProductGroupCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.RowVersionString).NotEmpty();
        }
    }

    public class DeleteProductGroupCommandValidator : AbstractValidator<DeleteProductGroupCommand>
    {
        public DeleteProductGroupCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}
