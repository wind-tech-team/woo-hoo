﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WooHoo.Domain.Events;

namespace WooHoo.WebApp.Application.DomainEventHandlers
{
    public class TestEventHandler : INotificationHandler<TestEvent>
    {
        public Task Handle(TestEvent notification, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
