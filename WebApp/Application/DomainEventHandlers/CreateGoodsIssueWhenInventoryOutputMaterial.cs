﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WooHoo.Domain.Aggregates.GoodsIssueAggregate;
using WooHoo.Domain.Events.InventoryEvents;

namespace WooHoo.WebApp.Application.DomainEventHandlers
{
    public class CreateGoodsIssueWhenInventoryOutputMaterial : INotificationHandler<InventoryMaterialOutputEvent>
    {
        private readonly IGoodsIssueRepository _goodsIssueRepo;

        public CreateGoodsIssueWhenInventoryOutputMaterial(IGoodsIssueRepository goodsIssueRepo)
        {
            _goodsIssueRepo = goodsIssueRepo;
        }

        public async Task Handle(InventoryMaterialOutputEvent notification, CancellationToken cancellationToken)
        {
            var issue = new GoodsIssue(notification.IssueDate, notification.InventoryId, notification.Note);
            notification.ItemsToOutput.Select(x =>
            {
                var materialItem = notification.DeductedItemsInInventory.Single(m => m.Id == x.ItemId);
                return new GoodsIssueItem(materialItem.MaterialId, x.Quantity, materialItem.ReceiptDate, materialItem.SupplierId, materialItem.ConsignmentCode);
            }).ToList().ForEach(issue.AddItem);

            await _goodsIssueRepo.InsertAsync(issue);
        }
    }
}
