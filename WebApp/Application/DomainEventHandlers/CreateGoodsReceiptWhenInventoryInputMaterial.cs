﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
using WooHoo.Domain.Events.InventoryEvents;

namespace WooHoo.WebApp.Application.DomainEventHandlers
{
    public class CreateGoodsReceiptWhenInventoryInputMaterial : INotificationHandler<InventoryMaterialInputEvent>
    {
        private readonly IGoodsReceiptRepository _goodsReceiptRepo;

        public CreateGoodsReceiptWhenInventoryInputMaterial(IGoodsReceiptRepository goodsReceiptRepo)
        {
            _goodsReceiptRepo = goodsReceiptRepo;
        }

        public async Task Handle(InventoryMaterialInputEvent notification, CancellationToken cancellationToken)
        {
            var receipt = new GoodsReceipt(notification.ReceiptDate, notification.InventoryId, notification.Price, notification.Note);
            var items = notification.Items.Select(x => new GoodsReceiptItem(x.MaterialId, x.Quantity, x.SupplierId, x.ConsignmentCode));
            foreach (var item in items)
            {
                receipt.AddItem(item);
            }

            await _goodsReceiptRepo.InsertAsync(receipt);
        }
    }
}
