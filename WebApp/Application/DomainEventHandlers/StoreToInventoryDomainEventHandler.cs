﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using WooHoo.Common.Helpers;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Events;
using WooHoo.Domain.Events.StoreEvents;
using WooHoo.Domain.Exceptions;

namespace WooHoo.WebApp.Application.DomainEventHandlers
{
    public class StoreToInventoryDomainEventHandler : INotificationHandler<StoreCreatedEvent>, INotificationHandler<StoreUpdatedEvent>
    {
        private IInventoryRepository _inventoryRepository;

        public StoreToInventoryDomainEventHandler(IInventoryRepository inventoryRepository)
        {
            this._inventoryRepository = inventoryRepository;
        }

        public async Task Handle(StoreCreatedEvent notification, CancellationToken cancellationToken)
        {
            if (notification.InventoryId == 0)
            {
                var inventory = new Inventory($"Kho - {notification.StoreName}", null);
                inventory.AssignToStore(notification.StoreId);
                await _inventoryRepository.InsertAsync(inventory);
            }
            else
            {
                var inventory = await _inventoryRepository.GetByIdAsync(notification.InventoryId);

                if (inventory == null)
                {
                    var validationEx = new ValidationException("Validation exception", new ValidationFailure[] { new ValidationFailure("InventoryId", $"Cannot find inventory with Id {notification.InventoryId}") });
                    throw new DomainException($"Domain event handler validation failed for {typeof(StoreToInventoryDomainEventHandler)}", validationEx);
                }

                inventory.AssignToStore(notification.StoreId);
                _inventoryRepository.Update(inventory, ByteArrayConverter.ToString(inventory.RowVersion));
            }

        }

        public async Task Handle(StoreUpdatedEvent notification, CancellationToken cancellationToken)
        {
            var inventory = await _inventoryRepository.GetByIdAsync(notification.InventoryId);

            if (inventory == null)
            {
                var validationEx = new ValidationException("Validation exception", new ValidationFailure[] { new ValidationFailure("InventoryId", $"Cannot find inventory with Id {notification.InventoryId}") });
                throw new DomainException($"Domain event handler validation failed for {typeof(StoreToInventoryDomainEventHandler)}", validationEx);
            }

            var oldInventoryOfStore = await _inventoryRepository.GetInventoryByStoreIdAsync(notification.StoreId);
            if (oldInventoryOfStore != null)
            {
                oldInventoryOfStore.RemoveFromStore();
                _inventoryRepository.Update(oldInventoryOfStore, ByteArrayConverter.ToString(oldInventoryOfStore.RowVersion));
            }

            inventory.AssignToStore(notification.StoreId);

            _inventoryRepository.Update(inventory, ByteArrayConverter.ToString(inventory.RowVersion));
        }
    }
}
