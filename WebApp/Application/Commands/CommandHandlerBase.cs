﻿using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Seedwork;
using MediatR;

namespace WooHoo.WebApp.Application.Commands
{
    public abstract class CommandHandlerBase<TCommand, TResult> : IRequestHandler<TCommand, TResult> 
        where TCommand : IRequest<TResult>
    {
        protected ServiceContext ServiceContext;

        protected CommandHandlerBase(ServiceContext serviceContext)
        {
            ServiceContext = serviceContext;
        }

        public abstract Task<TResult> Handle(TCommand rq, CancellationToken cancellationToken);
    }
}
