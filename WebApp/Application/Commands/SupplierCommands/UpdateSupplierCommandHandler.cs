﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.SupplierAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.SupplierCommands
{
    public class UpdateSupplierCommandHandler : CommandHandlerBase<UpdateSupplierCommand, int>
    {
        private readonly ISupplierRepository _supplierRepo;

        public UpdateSupplierCommandHandler(ISupplierRepository supplierRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _supplierRepo = supplierRepo;
        }

        public override async Task<int> Handle(UpdateSupplierCommand rq, CancellationToken cancellationToken)
        {
            var Supplier = await _supplierRepo.GetByIdAsync(rq.Id);
            Supplier.Set(rq.Name, rq.Description);

            _supplierRepo.Update(Supplier, rq.RowVersionString);
            await _supplierRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return Supplier.Id;
        }
    }
}
