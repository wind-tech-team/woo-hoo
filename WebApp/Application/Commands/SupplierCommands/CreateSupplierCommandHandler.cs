﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.SupplierAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.SupplierCommands
{
    public class CreateSupplierCommandHandler : CommandHandlerBase<CreateSupplierCommand, int>
    {
        private readonly ISupplierRepository _supplierRepo;

        public CreateSupplierCommandHandler(ISupplierRepository supplierRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _supplierRepo = supplierRepo;
        }

        public override async Task<int> Handle(CreateSupplierCommand rq, CancellationToken cancellationToken)
        {
            var supplier = new Supplier(rq.Name, rq.Description);

            await _supplierRepo.InsertAsync(supplier);
            await _supplierRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return supplier.Id;
        }
    }
}
