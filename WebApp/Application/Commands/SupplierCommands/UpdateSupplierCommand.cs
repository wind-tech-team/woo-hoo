﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace WooHoo.WebApp.Application.Commands.SupplierCommands
{
    public class UpdateSupplierCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }

        public string Description { get; }

        public string RowVersionString { get; }

        public UpdateSupplierCommand(int id, string name, string description, string rowVersionString)
        {
            Id = id;
            Name = name;
            Description = description;
            RowVersionString = rowVersionString;
        }
    }
}
