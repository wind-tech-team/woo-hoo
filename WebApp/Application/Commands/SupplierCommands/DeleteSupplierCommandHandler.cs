﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.SupplierAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.SupplierCommands
{
    public class DeleteSupplierCommandHandler : CommandHandlerBase<DeleteSupplierCommand, bool>
    {
        private readonly ISupplierRepository _supplierRepo;

        public DeleteSupplierCommandHandler(ISupplierRepository supplierRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _supplierRepo = supplierRepo;
        }

        public override async Task<bool> Handle(DeleteSupplierCommand rq, CancellationToken cancellationToken)
        {
            var store = await _supplierRepo.GetByIdAsync(rq.Id);

            _supplierRepo.Delete(store, rq.RowVersionString);
            await _supplierRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
