﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands
{
    public class DeleteGoodsReceiptCommandHandler : CommandHandlerBase<DeleteGoodsReceiptCommand, bool>
    {
        private readonly IGoodsReceiptRepository _goodsReceiptRepo;

        public DeleteGoodsReceiptCommandHandler(ServiceContext serviceContext, IGoodsReceiptRepository goodsReceiptRepo) : base(serviceContext)
        {
            _goodsReceiptRepo = goodsReceiptRepo;
        }

        public override async Task<bool> Handle(DeleteGoodsReceiptCommand rq, CancellationToken cancellationToken)
        {
            var receipt = await _goodsReceiptRepo.GetByIdAsync(rq.Id);

            _goodsReceiptRepo.Delete(receipt, rq.RowVersionString);
            await _goodsReceiptRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
