﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;
//using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
//using WooHoo.Domain.Seedwork;

//namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands
//{
//    public class UpdateGoodsReceiptCommandHandler : CommandHandlerBase<UpdateGoodsReceiptCommand, int>
//    {
//        private readonly IGoodsReceiptRepository _goodsReceiptRepo;

//        public UpdateGoodsReceiptCommandHandler(ServiceContext serviceContext, IGoodsReceiptRepository goodsReceiptRepo) : base(serviceContext)
//        {
//            _goodsReceiptRepo = goodsReceiptRepo;
//        }

//        public override async Task<int> Handle(UpdateGoodsReceiptCommand rq, CancellationToken cancellationToken)
//        {
//            var receipt = await _goodsReceiptRepo.GetByIdAsync(rq.Id);
//            receipt.Set(rq.ReceiptDate, rq.InventoryId, rq.Code);

//            // remove items
//            receipt.Items.Where(curItem => rq.Items.All(newItem => newItem.Id != curItem.Id)).ToList()
//                .ForEach(receipt.RemoveItem);

//            // update items
//            rq.Items.Where(newItem => receipt.Items.Any(curItem => curItem.Id == newItem.Id))
//                .Select(x => new KeyValuePair<int, GoodsReceiptItem>(x.Id, new GoodsReceiptItem(x.MaterialId, x.Quantity))).ToList()
//                .ForEach(x => receipt.UpdateItem(x.Key, x.Value));

//            // add new items
//            rq.Items.Where(x => x.Id == 0).Select(x => new GoodsReceiptItem(x.MaterialId, x.Quantity, x.SupplierId)).ToList()
//                .ForEach(receipt.AddItem);

//            _goodsReceiptRepo.Update(receipt, rq.RowVersionString);
//            await _goodsReceiptRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

//            return receipt.Id;
//        }
//    }
//}
