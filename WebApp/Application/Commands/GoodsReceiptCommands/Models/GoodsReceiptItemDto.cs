﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands.Models
{
    public class GoodsReceiptItemDto
    {
        public int Id { get; set; }

        public int MaterialId { get; set; }

        public decimal Quantity { get; set; }

        public int? SupplierId { get; set; }
    }
}
