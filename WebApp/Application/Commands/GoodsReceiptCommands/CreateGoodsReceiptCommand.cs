﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.WebApp.Application.Commands.GoodsReceiptCommands.Models;

namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands
{
    public class CreateGoodsReceiptCommand : IRequest<int>
    {
        public DateTime ReceiptDate { get; }

        public int InventoryId { get; }

        public decimal Price { get; }

        public string Note { get; set; }

        public IReadOnlyCollection<GoodsReceiptItemDto> Items { get; }

        public CreateGoodsReceiptCommand(DateTime receiptDate, int inventoryId, decimal price, List<GoodsReceiptItemDto> items, string note = null)
        {
            ReceiptDate = receiptDate;
            InventoryId = inventoryId;
            Price = price;
            Note = note;
            Items = items;
        }
    }
}
