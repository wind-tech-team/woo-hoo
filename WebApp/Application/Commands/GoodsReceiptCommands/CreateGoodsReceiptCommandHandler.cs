﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.GoodsReceiptAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands
{
    public class CreateGoodsReceiptCommandHandler : CommandHandlerBase<CreateGoodsReceiptCommand, int>
    {
        private readonly IGoodsReceiptRepository _goodsReceiptRepo;

        public CreateGoodsReceiptCommandHandler(ServiceContext serviceContext, IGoodsReceiptRepository goodsReceiptRepo) : base(serviceContext)
        {
            _goodsReceiptRepo = goodsReceiptRepo;
        }

        public override async Task<int> Handle(CreateGoodsReceiptCommand rq, CancellationToken cancellationToken)
        {
            var receipt = new GoodsReceipt(rq.ReceiptDate, rq.InventoryId, rq.Price, rq.Note);
            var items = rq.Items.Select(x => new GoodsReceiptItem(x.MaterialId, x.Quantity, x.SupplierId));
            foreach (var item in items)
            {
                receipt.AddItem(item);
            }

            await _goodsReceiptRepo.InsertAsync(receipt);
            await _goodsReceiptRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return receipt.Id;
        }
    }
}
