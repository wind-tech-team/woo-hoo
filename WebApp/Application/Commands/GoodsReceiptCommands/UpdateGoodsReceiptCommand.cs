﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.WebApp.Application.Commands.GoodsReceiptCommands.Models;

namespace WooHoo.WebApp.Application.Commands.GoodsReceiptCommands
{
    public class UpdateGoodsReceiptCommand : IRequest<int>
    {
        public int Id { get; set; }

        public DateTime ReceiptDate { get; }

        public int InventoryId { get; }

        public int? SupplierId { get; }

        public string Code { get; }

        public IReadOnlyCollection<GoodsReceiptItemDto> Items { get; }

        public string RowVersionString { get; }

        public UpdateGoodsReceiptCommand(DateTime receiptDate, int inventoryId, string code, List<GoodsReceiptItemDto> items, string rowVersionString, int? supplierId)
        {
            ReceiptDate = receiptDate;
            InventoryId = inventoryId;
            SupplierId = supplierId;
            Code = code;
            Items = items;
            RowVersionString = rowVersionString;
        }
    }
}
