﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Domain.Events;
using WooHoo.Domain.Events.StoreEvents;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class UpdateStoreCommandHandler : CommandHandlerBase<UpdateStoreCommand, int>
    {
        private readonly IStoreRepository _storeRepo;

        public UpdateStoreCommandHandler(IStoreRepository storeRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _storeRepo = storeRepo;
        }

        public override async Task<int> Handle(UpdateStoreCommand rq, CancellationToken cancellationToken)
        {
            var store = await _storeRepo.GetByIdAsync(rq.Id);
            store.Set(rq.Name, rq.Hotline, rq.Address);

            _storeRepo.Update(store, rq.RowVersionString);

            store.AddDomainEvent(new StoreUpdatedEvent(store.Id, rq.InventoryId));

            await _storeRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return store.Id;
        }
    }
}
