﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.Domain.ValueObjects;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class UpdateStoreCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }

        public string Hotline { get; }

        public Address Address { get; }

        public int InventoryId { get; }

        public string RowVersionString { get; }

        public UpdateStoreCommand(int id, string name, string hotline, Address address, int inventoryId, string rowVersionString)
        {
            Id = id;
            Name = name;
            Hotline = hotline;
            Address = address;
            InventoryId = inventoryId;
            RowVersionString = rowVersionString;
        }
    }
}
