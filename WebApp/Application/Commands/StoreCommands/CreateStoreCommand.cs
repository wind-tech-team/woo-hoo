﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.Domain.ValueObjects;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class CreateStoreCommand : IRequest<int>
    {
        public string Name { get; }

        public string Hotline { get; }

        public Address Address { get; }

        public int InventoryId { get; }

        public CreateStoreCommand(string name, string hotline, Address address, int inventoryId)
        {
            Name = name;
            Hotline = hotline;
            Address = address;
            InventoryId = inventoryId;
        }
    }
}
