﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class DeleteStoresCommandHandler : CommandHandlerBase<DeleteStoresCommand, bool>
    {
        private readonly IStoreRepository _storeRepo;

        public DeleteStoresCommandHandler(IStoreRepository storeRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _storeRepo = storeRepo;
        }

        public override async Task<bool> Handle(DeleteStoresCommand rq, CancellationToken cancellationToken)
        {
            var deleteTasks = new List<Task>();
            foreach (var item in rq.Items)
            {
                var task = Task.Run(async () =>
                {
                    var store = await _storeRepo.GetByIdAsync(item.Id);
                    _storeRepo.Delete(store, item.RowVersionString);
                }, cancellationToken);

                deleteTasks.Add(task);
            }

            await Task.WhenAll(deleteTasks);
           
            await _storeRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
