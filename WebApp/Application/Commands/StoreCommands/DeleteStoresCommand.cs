﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.WebApp.Application.Commands.Models;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class DeleteStoresCommand : IRequest<bool>
    {
        public List<DeleteModel> Items { get; }

        public DeleteStoresCommand(List<DeleteModel> items)
        {
            Items = items;
        }
    }
}
