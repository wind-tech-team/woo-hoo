﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class DeleteStoreCommandHandler : CommandHandlerBase<DeleteStoreCommand, bool>
    {
        private readonly IStoreRepository _storeRepo;

        public DeleteStoreCommandHandler(IStoreRepository storeRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _storeRepo = storeRepo;
        }

        public override async Task<bool> Handle(DeleteStoreCommand rq, CancellationToken cancellationToken)
        {
            var store = await _storeRepo.GetByIdAsync(rq.Id);

            _storeRepo.Delete(store, rq.RowVersionString);
            await _storeRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
