﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.StoreAggregate;
using WooHoo.Domain.Events;
using WooHoo.Domain.Events.StoreEvents;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.StoreCommands
{
    public class CreateStoreCommandHandler : CommandHandlerBase<CreateStoreCommand, int>
    {
        private readonly IStoreRepository _storeRepo;

        public CreateStoreCommandHandler(IStoreRepository storeRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _storeRepo = storeRepo;
        }

        public override async Task<int> Handle(CreateStoreCommand rq, CancellationToken cancellationToken)
        {
            var store = new Store(rq.Name, rq.Hotline, rq.Address);

            await _storeRepo.InsertAsync(store);

            store.AddDomainEvent(new StoreCreatedEvent(store.Id, store.Name, rq.InventoryId));

            await _storeRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return store.Id;
        }
    }
}
