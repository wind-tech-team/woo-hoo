﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.EmployeeAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class CreateEmployeeCommandHandler : CommandHandlerBase<CreateEmployeeCommand, int>
    {
        private readonly IEmployeeReposiory _employeeReposiory;
        public CreateEmployeeCommandHandler(IEmployeeReposiory employeeReposiory, ServiceContext serviceContext) 
            : base(serviceContext)
        {
            this._employeeReposiory = employeeReposiory;
        }

        public override async Task<int> Handle(CreateEmployeeCommand rq, CancellationToken cancellationToken)
        {
            var employee = new Employee(rq.Name, rq.Phone, rq.Address, rq.DateOfBirth);
            await this._employeeReposiory.InsertAsync(employee);
            await this._employeeReposiory.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return employee.Id;
        }
    }
}
