﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.EmployeeAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class DeleteEmployeeCommandHandler : CommandHandlerBase<DeleteEmployeeCommand, bool>
    {
        private readonly IEmployeeReposiory _employeeReposiory;

        public DeleteEmployeeCommandHandler(IEmployeeReposiory employeeReposiory, ServiceContext serviceContext) : base(serviceContext)
        {
            this._employeeReposiory = employeeReposiory;
        }

        public override async Task<bool> Handle(DeleteEmployeeCommand rq, CancellationToken cancellationToken)
        {
            var employee = await _employeeReposiory.GetByIdAsync(rq.Id);

            _employeeReposiory.Delete(employee, rq.RowVersionString);
            await _employeeReposiory.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
