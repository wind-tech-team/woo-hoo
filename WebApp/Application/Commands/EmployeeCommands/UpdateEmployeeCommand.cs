﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class UpdateEmployeeCommand : IRequest<int>
    {
        public int Id { get; }
        public string Name { get; }
        public string Phone { get; }

        public string Address { get; }

        public DateTime? DateOfBirth { get; }
        public string RowVersionString { get; }

        public UpdateEmployeeCommand(int id, string name, string phone, string address, DateTime? dateOfBirth, string rowVersionString)
        {
            this.Id = id;
            this.Name = name;
            this.Phone = phone;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
            this.RowVersionString = rowVersionString;
        }
    }
}
