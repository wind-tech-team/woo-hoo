﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.EmployeeAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class UpdateEmployeeCommandHandler : CommandHandlerBase<UpdateEmployeeCommand, int>
    {
        private readonly IEmployeeReposiory _employeeReposiory;

        public UpdateEmployeeCommandHandler(IEmployeeReposiory employeeReposiory, ServiceContext serviceContext) : base(serviceContext)
        {
            this._employeeReposiory = employeeReposiory;
        }

        public override async Task<int> Handle(UpdateEmployeeCommand rq, CancellationToken cancellationToken)
        {
            var employee = await _employeeReposiory.GetByIdAsync(rq.Id);

            employee.Set(rq.Name, rq.Phone, rq.Address, rq.DateOfBirth);

            _employeeReposiory.Update(employee, rq.RowVersionString);
            await _employeeReposiory.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return employee.Id;
        }
    }
}
