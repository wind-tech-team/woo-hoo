﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class CreateEmployeeCommand : IRequest<int>
    {
        public string Name { get; }
        public string Phone { get; }
        public string Address { get; }
        public DateTime? DateOfBirth { get; }

        public CreateEmployeeCommand(string name, string phone, string address, DateTime? dateOfBirth)
        {
            this.Name = name;
            this.Phone = phone;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
        }
    }
}
