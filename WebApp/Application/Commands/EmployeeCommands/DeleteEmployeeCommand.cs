﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.EmployeeCommands
{
    public class DeleteEmployeeCommand : IRequest<bool>
    {
        public int Id { get; }

        public string RowVersionString { get; }

        public DeleteEmployeeCommand(int id, string rowVersionString)
        {
            Id = id;
            RowVersionString = rowVersionString;
        }
    }
}
