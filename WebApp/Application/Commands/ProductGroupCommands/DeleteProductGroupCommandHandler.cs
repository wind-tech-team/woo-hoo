﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.ProductGroupCommands
{
    public class DeleteProductGroupCommandHandler : CommandHandlerBase<DeleteProductGroupCommand, bool>
    {
        private readonly IProductGroupRepository _productGroupRepo;

        public DeleteProductGroupCommandHandler(IProductGroupRepository productGroupRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _productGroupRepo = productGroupRepo;
        }

        public override async Task<bool> Handle(DeleteProductGroupCommand rq, CancellationToken cancellationToken)
        {
            var store = await _productGroupRepo.GetByIdAsync(rq.Id);

            _productGroupRepo.Delete(store, rq.RowVersionString);
            await _productGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
