﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace WooHoo.WebApp.Application.Commands.ProductGroupCommands
{
    public class DeleteProductGroupCommand : IRequest<bool>
    {
        public int Id { get; }

        public string RowVersionString { get; }

        public DeleteProductGroupCommand(int id, string rowVersionString)
        {
            Id = id;
            RowVersionString = rowVersionString;
        }
    }
}
