﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace WooHoo.WebApp.Application.Commands.ProductGroupCommands
{
    public class CreateProductGroupCommand : IRequest<int>
    {
        public string Name { get; }

        public string Description { get; }

        public CreateProductGroupCommand(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
