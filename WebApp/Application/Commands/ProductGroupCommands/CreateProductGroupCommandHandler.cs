﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.ProductGroupCommands
{
    public class CreateProductGroupCommandHandler : CommandHandlerBase<CreateProductGroupCommand, int>
    {
        private readonly IProductGroupRepository _productGroupRepo;

        public CreateProductGroupCommandHandler(IProductGroupRepository productGroupRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _productGroupRepo = productGroupRepo;
        }

        public override async Task<int> Handle(CreateProductGroupCommand rq, CancellationToken cancellationToken)
        {
            var productGroup = new ProductGroup(rq.Name, rq.Description);

            await _productGroupRepo.InsertAsync(productGroup);
            await _productGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return productGroup.Id;
        }
    }
}
