﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.ProductGroupCommands
{
    public class UpdateProductGroupCommandHandler : CommandHandlerBase<UpdateProductGroupCommand, int>
    {
        private readonly IProductGroupRepository _productGroupRepo;

        public UpdateProductGroupCommandHandler(IProductGroupRepository productGroupRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _productGroupRepo = productGroupRepo;
        }

        public override async Task<int> Handle(UpdateProductGroupCommand rq, CancellationToken cancellationToken)
        {
            var ProductGroup = await _productGroupRepo.GetByIdAsync(rq.Id);
            ProductGroup.Set(rq.Name, rq.Description);

            _productGroupRepo.Update(ProductGroup, rq.RowVersionString);
            await _productGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return ProductGroup.Id;
        }
    }
}
