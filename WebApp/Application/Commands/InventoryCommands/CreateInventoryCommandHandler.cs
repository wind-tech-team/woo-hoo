﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class CreateInventoryCommandHandler : CommandHandlerBase<CreateInventoryCommand, int>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public CreateInventoryCommandHandler(IInventoryRepository inventoryRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<int> Handle(CreateInventoryCommand rq, CancellationToken cancellationToken)
        {
            var inventory = new Inventory(rq.Name, rq.Address);
            
            await _inventoryRepo.InsertAsync(inventory);
            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return inventory.Id;
        }
    }
}
