﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.WebApp.Application.Commands.InventoryCommands.Models;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class OutputMaterialCommand : IRequest<bool>
    {
        public int InventoryId { get; }

        public DateTime IssueDate { get; }

        public string Note { get; }

        public IReadOnlyCollection<OutputMaterialItemDto> Items { get; }

        public OutputMaterialCommand(int inventoryId, DateTime issueDate, string note, List<OutputMaterialItemDto> items)
        {
            InventoryId = inventoryId;
            IssueDate = issueDate.Date;
            Note = note;
            Items = items;
        }
    }
}
