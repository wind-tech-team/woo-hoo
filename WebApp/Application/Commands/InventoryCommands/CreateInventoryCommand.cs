﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class CreateInventoryCommand : IRequest<int>
    {
        public string Name { get; }

        public string Address { get; }

        public CreateInventoryCommand(string name, string address)
        {
            Name = name;
            Address = address;
        }

    }
}
