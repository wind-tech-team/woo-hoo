﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class UpdateInventoryCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }
        
        public string Address { get; }

        public string RowVersionString { get; }

        public UpdateInventoryCommand(int id, string name, string address, string rowVersionString)
        {
            Id = id;
            Name = name;
            Address = address;
            RowVersionString = rowVersionString;
        }

    }
}
