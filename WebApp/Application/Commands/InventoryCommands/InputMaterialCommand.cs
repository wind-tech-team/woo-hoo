﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using WooHoo.WebApp.Application.Commands.InventoryCommands.Models;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class InputMaterialCommand : IRequest<bool>
    {
        public int InventoryId { get; }

        public DateTime ReceiptDate { get; }

        public decimal Price { get; }

        public string Note { get; }

        public IReadOnlyCollection<InputMaterialItemDto> Items { get; }

        public InputMaterialCommand(int inventoryId, DateTime receiptDate, decimal price, string note, List<InputMaterialItemDto> items)
        {
            InventoryId = inventoryId;
            ReceiptDate = receiptDate.Date;
            Note = note;
            Price = price;
            Items = items;
        }
    }
}
