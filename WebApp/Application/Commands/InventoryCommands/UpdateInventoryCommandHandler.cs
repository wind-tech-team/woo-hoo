﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class UpdateInventoryCommandHandler : CommandHandlerBase<UpdateInventoryCommand, int>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public UpdateInventoryCommandHandler(IInventoryRepository inventoryRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<int> Handle(UpdateInventoryCommand rq, CancellationToken cancellationToken)
        {
            var inventory = await _inventoryRepo.GetByIdAsync(rq.Id);
            inventory.Set(rq.Name, rq.Address);
            
            _inventoryRepo.Update(inventory, rq.RowVersionString);
            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return inventory.Id;
        }
    }
}
