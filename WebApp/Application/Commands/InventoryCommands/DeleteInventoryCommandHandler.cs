﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class DeleteInventoryCommandHandler : CommandHandlerBase<DeleteInventoryCommand, bool>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public DeleteInventoryCommandHandler(IInventoryRepository inventoryRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<bool> Handle(DeleteInventoryCommand rq, CancellationToken cancellationToken)
        {
            var inventory = await _inventoryRepo.GetByIdAsync(rq.Id);

            if (inventory.StoreId.HasValue)
            {
                var validationEx = new ValidationException("Validation exception", new ValidationFailure[] { new ValidationFailure(nameof(inventory.StoreId), "Cannot delete inventory of a store") });
                throw new DomainException($"Command Validation Errors for type {typeof(DeleteInventoryCommandHandler).Name}", validationEx);
            }

            _inventoryRepo.Delete(inventory, rq.RowVersionString);
            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
