﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands.Models
{
    public class OutputMaterialItemDto
    {
        public int MaterialItemId { get; set; }

        public decimal Quantity { get; set; }
    }
}
