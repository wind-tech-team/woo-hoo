﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands.Models
{
    public class InputMaterialItemDto
    {
        public int MaterialId { get; set; }

        public int? SupplierId { get; set; }

        public string ConsignmentCode { get; set; }

        public decimal Quantity { get; set; }
    }
}
