﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Events.InventoryEvents;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class InputMaterialCommandHandler : CommandHandlerBase<InputMaterialCommand, bool>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public InputMaterialCommandHandler(ServiceContext serviceContext, IInventoryRepository inventoryRepo) : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<bool> Handle(InputMaterialCommand rq, CancellationToken cancellationToken)
        {
            var inventory = await _inventoryRepo.GetByIdAsync(rq.InventoryId);
            if (inventory == null)
            {
                throw new DomainException("Invalid Inventory");
            }

            // input items into inventory
            var materialItems = rq.Items.Select(x => new MaterialInventoryItem(x.MaterialId, x.Quantity, rq.ReceiptDate, x.SupplierId, x.ConsignmentCode)).ToList();
            inventory.InputMaterials(materialItems);

            // raise input event
            inventory.AddDomainEvent(new InventoryMaterialInputEvent(rq.ReceiptDate, rq.InventoryId, rq.Price, rq.Note, materialItems));

            // update inventory
            _inventoryRepo.Update(inventory, inventory.RowVersion);
            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
