﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Events.InventoryEvents;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class OutputMaterialCommandHandler : CommandHandlerBase<OutputMaterialCommand, bool>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public OutputMaterialCommandHandler(ServiceContext serviceContext, IInventoryRepository inventoryRepo) : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<bool> Handle(OutputMaterialCommand rq, CancellationToken cancellationToken)
        {
            var inventory = await _inventoryRepo.GetByIdAsync(rq.InventoryId);
            if (inventory == null)
            {
                throw new RecordNotFoundException(nameof(Inventory));
            }

            // output items from inventory
            var itemsToOutput = rq.Items.Select(x => new Inventory.InventoryOutputRequest(x.MaterialItemId, x.Quantity)).ToList();
            var deductedItemsInInventory = inventory.OutputMaterials(itemsToOutput).ToList();

            // raise output event
            inventory.AddDomainEvent(new InventoryMaterialOutputEvent(rq.IssueDate, rq.InventoryId, rq.Note, itemsToOutput, deductedItemsInInventory));

            // update inventory
            _inventoryRepo.Update(inventory, inventory.RowVersion);
            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
