﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class DeleteInventoriesCommandHandler : CommandHandlerBase<DeleteInventoriesCommand, bool>
    {
        private readonly IInventoryRepository _inventoryRepo;

        public DeleteInventoriesCommandHandler(IInventoryRepository inventoryRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _inventoryRepo = inventoryRepo;
        }

        public override async Task<bool> Handle(DeleteInventoriesCommand rq, CancellationToken cancellationToken)
        {
            var deleteTasks = new List<Task>();
            foreach (var item in rq.Items)
            {
                var task = Task.Run(async () =>
                {
                    var inventory = await _inventoryRepo.GetByIdAsync(item.Id);

                    if (inventory.StoreId.HasValue)
                    {
                        var validationEx = new ValidationException("Validation exception", new ValidationFailure[] { new ValidationFailure(nameof(inventory.StoreId), "Cannot delete inventory of a store") });
                        throw new DomainException($"Command Validation Errors for type {typeof(DeleteInventoryCommandHandler).Name}", validationEx);
                    }

                    _inventoryRepo.Delete(inventory, item.RowVersionString);
                }, cancellationToken);

                deleteTasks.Add(task);
            }

            await Task.WhenAll(deleteTasks);

            await _inventoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
