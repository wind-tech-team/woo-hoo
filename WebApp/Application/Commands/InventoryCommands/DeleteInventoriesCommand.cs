﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.WebApp.Application.Commands.Models;

namespace WooHoo.WebApp.Application.Commands.InventoryCommands
{
    public class DeleteInventoriesCommand : IRequest<bool>
    {
        public List<DeleteModel> Items { get; }

        public DeleteInventoriesCommand(List<DeleteModel> items)
        {
            Items = items;
        }
    }
}
