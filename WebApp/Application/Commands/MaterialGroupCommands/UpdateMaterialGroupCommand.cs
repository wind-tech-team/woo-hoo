﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.MaterialGroupCommands
{
    public class UpdateMaterialGroupCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }

        public string Description { get; }

        public string RowVersionString { get; }

        public UpdateMaterialGroupCommand(int id, string name, string description, 
            string rowVersionString)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.RowVersionString = rowVersionString;
        }
    }
}
