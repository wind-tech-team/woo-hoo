﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.MaterialGroupCommands
{
    public class CreateMaterialGroupCommand : IRequest<int>
    {
        public string Name { get; }
        public string Description { get; }
        public CreateMaterialGroupCommand(string name,string description)
        {
            this.Name = name;
            this.Description = description;
        }
    }
}
