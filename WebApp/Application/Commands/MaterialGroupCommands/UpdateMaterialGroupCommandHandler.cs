﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialGroupCommands
{
    public class UpdateMaterialGroupCommandHandler : CommandHandlerBase<UpdateMaterialGroupCommand, int>
    {
        private readonly IMaterialGroupRepository _materialGroupRepo;

        public UpdateMaterialGroupCommandHandler(IMaterialGroupRepository materialGroupRepo, ServiceContext serviceContext) : base(serviceContext)
        {
            this._materialGroupRepo = materialGroupRepo;
        }

        public override async Task<int> Handle(UpdateMaterialGroupCommand rq, CancellationToken cancellationToken)
        {
            var materialGroup = await _materialGroupRepo.GetByIdAsync(rq.Id);
            materialGroup.Set(rq.Name, rq.Description);

            _materialGroupRepo.Update(materialGroup, rq.RowVersionString);
            await _materialGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return materialGroup.Id;
        }
    }
}
