﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialGroupCommands
{
    public class DeleteMaterialGroupCommandHandler : CommandHandlerBase<DeleteMaterialGroupCommand, bool>
    {
        private readonly IMaterialGroupRepository _materialGroupRepo;

        public DeleteMaterialGroupCommandHandler(IMaterialGroupRepository materialGroupRepo, ServiceContext serviceContext) : base(serviceContext)
        {
            this._materialGroupRepo = materialGroupRepo;
        }

        public override async Task<bool> Handle(DeleteMaterialGroupCommand rq, CancellationToken cancellationToken)
        {
            var materialGroup = await _materialGroupRepo.GetByIdAsync(rq.Id);

            _materialGroupRepo.Delete(materialGroup, rq.RowVersionString);
            await _materialGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
