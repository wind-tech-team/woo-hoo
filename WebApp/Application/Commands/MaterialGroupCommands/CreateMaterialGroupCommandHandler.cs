﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialGroupCommands
{
    public class CreateMaterialGroupCommandHandler : CommandHandlerBase<CreateMaterialGroupCommand, int>
    {
        private readonly IMaterialGroupRepository _materialGroupRepo;

        public CreateMaterialGroupCommandHandler(IMaterialGroupRepository materialGroupRepo,ServiceContext serviceContext) 
            : base(serviceContext)
        {
            _materialGroupRepo = materialGroupRepo;
        }

        public override async Task<int> Handle(CreateMaterialGroupCommand rq, CancellationToken cancellationToken)
        {
            var materialGroup = new MaterialGroup(rq.Name, rq.Description);
            await _materialGroupRepo.InsertAsync(materialGroup);
            await _materialGroupRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return materialGroup.Id;
        }
    }
}
