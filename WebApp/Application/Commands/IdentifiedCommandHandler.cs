﻿using System.Threading;
using System.Threading.Tasks;
using WooHoo.Infrastructure.Idempotency;
using MediatR;

namespace WooHoo.WebApp.Application.Commands
{
    public class IdentifiedCommandHandler<T, R> : IRequestHandler<IdentifiedCommand<T, R>, R>
        where T : IRequest<R>
    {
        private readonly IMediator _mediator;
        private readonly IRequestManager _requestManager;

        public IdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager)
        {
            _mediator = mediator;
            _requestManager = requestManager;
        }

        protected virtual R CreateResultForDuplicatedRequest()
        {
            return default(R);
        }

        public async Task<R> Handle(IdentifiedCommand<T, R> request, CancellationToken cancellationToken)
        {
            var alreadyExists = await _requestManager.ExistsAsync(request.Id);
            if (alreadyExists)
            {
                return CreateResultForDuplicatedRequest();
            }

            await _requestManager.CreateRequestForCommandAsync<T>(request.Id);
            var result = await _mediator.Send(request.Command, cancellationToken);

            return result;
        }
    }
}
