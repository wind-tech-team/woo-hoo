﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.Models
{
    public class DeleteModel
    {
        public int Id { get; set; }

        public string RowVersionString { get; set; }
    }
}
