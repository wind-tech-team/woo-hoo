﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialCommands
{
    public class UpdateMaterialCommandHandler : CommandHandlerBase<UpdateMaterialCommand, int>
    {
        private readonly IMaterialRepository _materialRepo;

        public UpdateMaterialCommandHandler(IMaterialRepository materialRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _materialRepo = materialRepo;
        }

        public override async Task<int> Handle(UpdateMaterialCommand rq, CancellationToken cancellationToken)
        {
            var material = await _materialRepo.GetByIdAsync(rq.Id);
            material.Set(rq.Name, rq.InventoryUnit, rq.SaleUnit, rq.InventoryToSaleAmountPerUnit, rq.AveragePricePerInventoryUnit, rq.Description, rq.MinimumInStock);

            if (rq.MaterialGroupId.HasValue)
            {
                material.AssignToMaterialGroup(rq.MaterialGroupId.Value);
            }

            _materialRepo.Update(material, rq.RowVersionString);
            await _materialRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return material.Id;
        }
    }
}
