﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.MaterialCommands
{
    public class CreateMaterialCommand : IRequest<int>
    {
        public string Name { get; }

        public string Description { get; }

        public decimal MinimumInStock { get; }
        
        public string InventoryUnit { get; }
        
        public string SaleUnit { get; }
        
        public decimal InventoryToSaleAmountPerUnit { get; }
        
        public decimal AveragePricePerInventoryUnit { get; }

        public int? MaterialGroupId { get; }

        public CreateMaterialCommand(string name, string description, decimal minimumInStock, string inventoryUnit, string saleUnit, decimal inventoryToSaleAmountPerUnit, decimal averagePricePerInventoryUnit, int? materialGroupId)
        {
            Name = name;
            Description = description;
            MinimumInStock = minimumInStock;
            InventoryUnit = inventoryUnit;
            SaleUnit = saleUnit;
            InventoryToSaleAmountPerUnit = inventoryToSaleAmountPerUnit;
            AveragePricePerInventoryUnit = averagePricePerInventoryUnit;
            MaterialGroupId = materialGroupId;
        }

    }
}
