﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialCommands
{
    public class CreateMaterialCommandHandler : CommandHandlerBase<CreateMaterialCommand, int>
    {
        private readonly IMaterialRepository _materialRepo;

        public CreateMaterialCommandHandler(IMaterialRepository materialRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _materialRepo = materialRepo;
        }

        public override async Task<int> Handle(CreateMaterialCommand rq, CancellationToken cancellationToken)
        {
            var material = new Material(rq.Name, rq.InventoryUnit, rq.SaleUnit, rq.InventoryToSaleAmountPerUnit, rq.AveragePricePerInventoryUnit, rq.Description, rq.MinimumInStock);

            if (rq.MaterialGroupId.HasValue)
            {
                material.AssignToMaterialGroup(rq.MaterialGroupId.Value);
            }

            await _materialRepo.InsertAsync(material);
            await _materialRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return material.Id;
        }
    }
}
