﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Seedwork;

namespace WooHoo.WebApp.Application.Commands.MaterialCommands
{
    public class DeleteMaterialCommandHandler : CommandHandlerBase<DeleteMaterialCommand, bool>
    {
        private readonly IMaterialRepository _materialRepo;

        public DeleteMaterialCommandHandler(IMaterialRepository materialRepo, ServiceContext serviceContext)
            : base(serviceContext)
        {
            _materialRepo = materialRepo;
        }

        public override async Task<bool> Handle(DeleteMaterialCommand rq, CancellationToken cancellationToken)
        {
            var material = await _materialRepo.GetByIdAsync(rq.Id);

            _materialRepo.Delete(material, rq.RowVersionString);
            await _materialRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }
}
