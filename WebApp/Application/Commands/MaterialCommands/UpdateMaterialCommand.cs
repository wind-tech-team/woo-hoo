﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Commands.MaterialCommands
{
    public class UpdateMaterialCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }

        public string Description { get; }

        public decimal MinimumInStock { get; }

        public string InventoryUnit { get; }

        public string SaleUnit { get; }

        public decimal InventoryToSaleAmountPerUnit { get; }

        public decimal AveragePricePerInventoryUnit { get; }

        public int? MaterialGroupId { get; }

        public string RowVersionString { get; }

        public UpdateMaterialCommand(int id, string name, string description, decimal minimumInStock, string inventoryUnit, string saleUnit, decimal inventoryToSaleAmountPerUnit, decimal averagePricePerInventoryUnit, int? materialGroupId, string rowVersionString)
        {
            Id = id;
            Name = name;
            Description = description;
            MinimumInStock = minimumInStock;
            InventoryUnit = inventoryUnit;
            SaleUnit = saleUnit;
            InventoryToSaleAmountPerUnit = inventoryToSaleAmountPerUnit;
            AveragePricePerInventoryUnit = averagePricePerInventoryUnit;
            MaterialGroupId = materialGroupId;
            RowVersionString = rowVersionString;
        }

    }
}
