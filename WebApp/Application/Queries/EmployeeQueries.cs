﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.EmployeeAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IEmployeeQueries
    {
        Task<EmployeeReturnModel> GetEmployeeAsync(int id);
        Task<PagedList<EmployeeReturnModel>> GetEmployeesAsync(ListRequestModel rq);
    }

    public class EmployeeQueries : IEmployeeQueries
    {
        private readonly string _connectionString;
             
        public EmployeeQueries(IConfiguration configuration)
        {
            this._connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<EmployeeReturnModel> GetEmployeeAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT *
                              FROM Employees
                              WHERE Id = @id AND IsDeleted = 0";

                var employee = await connection.QueryFirstOrDefaultAsync<EmployeeReturnModel>(query, new { id });

                return employee;
            }
        }

        public async Task<PagedList<EmployeeReturnModel>> GetEmployeesAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetEmployeesFilterClause(rq);
                var orderByClause = BuildGetEmployeesOrderByClause(rq);

                var pagingQuery = $@"SELECT *
                                  FROM Employees
                                  WHERE {filterClause} AND IsDeleted = 0
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = @"SELECT COUNT(*)
                                  FROM Employees WHERE IsDeleted = 0";

                var employees = await connection.QueryAsync<EmployeeReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<EmployeeReturnModel> { Data = employees, RowsCount = rowsCount };
            }
        }

        private string BuildGetEmployeesFilterClause(ListRequestModel rq)
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({nameof(Employee.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {nameof(Employee.Phone)} LIKE '%{rq.SearchTerm}%')";
            }

            // final clause
            var filterClause = $"{searchTermClause}";

            return filterClause;
        }

        private string BuildGetEmployeesOrderByClause(ListRequestModel rq)
        {
            var orderByClause = "CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
