﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Common.Helpers;
using Newtonsoft.Json;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class StoreReturnModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Hotline { get; set; }

        public int? InventoryId { get; set; }

        public AddressReturnModel Address { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string RowVersionString => ByteArrayConverter.ToString(RowVersion);

        [JsonIgnore]
        public byte[] RowVersion { get; set; }
    }
}
