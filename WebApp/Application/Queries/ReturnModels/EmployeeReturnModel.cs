﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Common.Helpers;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class EmployeeReturnModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string RowVersionString => ByteArrayConverter.ToString(RowVersion);

        [JsonIgnore]
        public byte[] RowVersion { get; set; }
    }
}
