﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class DropDownModel
    {
        public string Value { get; set; }

        public string Label { get; set; }

        public string Tag { get; set; }
    }
}
