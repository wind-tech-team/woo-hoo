﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class PagedList<T>
    {
        public int RowsCount { get; set; }

        public IEnumerable<T> Data { get; set; }
    }
}
