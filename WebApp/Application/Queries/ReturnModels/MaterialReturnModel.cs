﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Common.Helpers;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class MaterialReturnModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        public decimal MinimumInStock { get; set; }
        
        public string InventoryUnit { get; set; }
        
        public string SaleUnit { get; set; }
        
        public decimal InventoryToSaleAmountPerUnit { get; set; }
        
        public decimal AveragePricePerInventoryUnit { get; set; }
        
        public decimal AverageCostPerSaleUnit { get; set; }

        public int? MaterialGroupId { get; set; }
        public string MaterialGroupName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string RowVersionString => ByteArrayConverter.ToString(RowVersion);

        [JsonIgnore]
        public byte[] RowVersion { get; set; }
    }
}
