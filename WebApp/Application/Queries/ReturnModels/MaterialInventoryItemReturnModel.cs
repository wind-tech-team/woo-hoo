﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class MaterialInventoryItemReturnModel
    {
        public int MaterialId { get; set; }

        public string MaterialName { get; set; }

        public decimal Quantity { get; set; }

        public DateTime ReceiptDate { get; set; }

        public int? SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string ConsignmentCode { get; set; }
    }
}
