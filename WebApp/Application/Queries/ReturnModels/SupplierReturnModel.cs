﻿using System;
using Newtonsoft.Json;
using WooHoo.Common.Helpers;

namespace WooHoo.WebApp.Application.Queries.ReturnModels
{
    public class SupplierReturnModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string RowVersionString => ByteArrayConverter.ToString(RowVersion);

        [JsonIgnore]
        public byte[] RowVersion { get; set; }
    }
}
