﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialAggregate;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IMaterialQueries
    {
        Task<MaterialReturnModel> GetMaterialAsync(int id);
        Task<PagedList<MaterialReturnModel>> GetMaterialsAsync(ListRequestModel rq);
        Task<List<DropDownModel>> GetMaterialDropdownListAsync();
    }

    public class MaterialQueries : IMaterialQueries
    {
        private readonly string _connectionString;

        public MaterialQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<MaterialReturnModel> GetMaterialAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT mat.*, matGroup.[Name] as MaterialGroupName
                              FROM Materials mat 
                                LEFT JOIN MaterialGroups matGroup ON mat.MaterialGroupId = matGroup.Id
                              WHERE mat.Id = @id AND IsDeleted = 0";

                var material = await connection.QueryFirstOrDefaultAsync<MaterialReturnModel>(query, new { id });

                return material;
            }
        }

        public async Task<PagedList<MaterialReturnModel>> GetMaterialsAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetMaterialsFilterClause(rq);
                var orderByClause = BuildGetMaterialsOrderByClause(rq);

                var pagingQuery = $@"SELECT mat.*, matGroup.[Name] as MaterialGroupName
                                  FROM Materials mat
                                    LEFT JOIN MaterialGroups matGroup ON mat.MaterialGroupId = matGroup.Id
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                  FROM Materials mat
                                    LEFT JOIN MaterialGroups matGroup ON mat.MaterialGroupId = matGroup.Id
                                  WHERE {filterClause}";

                var materials = await connection.QueryAsync<MaterialReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<MaterialReturnModel> { Data = materials, RowsCount = rowsCount };
            }
        }

        public async Task<List<DropDownModel>> GetMaterialDropdownListAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT Id as Value, Name as Label, InventoryUnit as Tag
                              FROM Materials 
                              WHERE IsDeleted = 0
                              ORDER BY Name";


                var materials = await connection.QueryAsync<DropDownModel>(query);

                return materials.ToList();
            }
        }

        private string BuildGetMaterialsFilterClause(ListRequestModel rq)
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"(mat.{nameof(Material.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR mat.{nameof(Material.Description)} LIKE '%{rq.SearchTerm}%'
                                        OR matGroup.{nameof(MaterialGroup.Name)} LIKE '%{rq.SearchTerm}%')";
            }

            // non-deleted clause
            var notDeletedClause = "mat.IsDeleted = 0";

            // final clause
            var filterClause = $"{notDeletedClause} AND {searchTermClause}";

            return filterClause;
        }

        private string BuildGetMaterialsOrderByClause(ListRequestModel rq)
        {
            var orderByClause = "CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
