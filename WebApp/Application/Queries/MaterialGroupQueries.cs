﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.MaterialGroupAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IMaterialGroupQueries
    {
        Task<MaterialGroupReturnModel> GetMaterialGroupAsync(int id);
        Task<PagedList<MaterialGroupReturnModel>> GetMaterialGroupsAsync(ListRequestModel rq);
        Task<IEnumerable<MaterialGroupReturnModel>> GetMaterialGroupsDropdownListAsync();
    }

    public class MaterialGroupQueries : IMaterialGroupQueries
    {
        private readonly string _connectionString;
        public MaterialGroupQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<MaterialGroupReturnModel> GetMaterialGroupAsync(int id)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();

                var query = @"SELECT *
                              FROM MaterialGroups
                              WHERE Id = @id";

                var materialGroup = await connection.QueryFirstOrDefaultAsync<MaterialGroupReturnModel>(query, new { id });

                return materialGroup;
            }
        }

        public async Task<PagedList<MaterialGroupReturnModel>> GetMaterialGroupsAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetMaterialGroupsFilterClause(rq);
                var orderByClause = BuildGetMaterialGroupsOrderByClause(rq);

                var pagingQuery = $@"SELECT *
                                  FROM MaterialGroups 
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                  FROM MaterialGroups
                                  WHERE {filterClause}";

                var materialGroups = await connection.QueryAsync<MaterialGroupReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<MaterialGroupReturnModel> { Data = materialGroups, RowsCount = rowsCount };
            }
        }

        public async Task<IEnumerable<MaterialGroupReturnModel>> GetMaterialGroupsDropdownListAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT *
                                  FROM MaterialGroups 
                                  ORDER BY Name";


                var materialGroups = await connection.QueryAsync<MaterialGroupReturnModel>(query);

                return materialGroups;
            }
        }

        private string BuildGetMaterialGroupsFilterClause(ListRequestModel rq)
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({nameof(MaterialGroup.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {nameof(MaterialGroup.Description)} LIKE '%{rq.SearchTerm}%')";
            }

            // final clause
            var filterClause = $"{searchTermClause}";

            return filterClause;
        }

        private string BuildGetMaterialGroupsOrderByClause(ListRequestModel rq)
        {
            var orderByClause = "CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
