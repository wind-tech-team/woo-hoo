﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;
using Microsoft.Extensions.Configuration;
using WooHoo.Domain.Aggregates.StoreAggregate;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IStoreQueries
    {
        Task<StoreReturnModel> GetStoreAsync(int id);
        Task<PagedList<StoreReturnModel>> GetStoresAsync(ListRequestModel rq);
    }

    public class StoreQueries : IStoreQueries
    {
        private readonly string _connectionString;

        public StoreQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<StoreReturnModel> GetStoreAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT inv.Id AS InventoryId, s.*
                              FROM Stores s LEFT JOIN Inventories inv ON s.Id = inv.StoreId
                              WHERE s.Id = @id AND s.IsDeleted = 0";

                var store = (await connection.QueryAsync<StoreReturnModel, AddressReturnModel, StoreReturnModel>(query, (s, a) =>
                {
                    s.Address = a;
                    return s;
                }, 
                splitOn: "Address_Street",
                param: new { id })).SingleOrDefault();

                return store;
            }
        }

        public async Task<PagedList<StoreReturnModel>> GetStoresAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetStoresFilterClause(rq, "s");
                var orderByClause = BuildGetStoresOrderByClause(rq, "s");

                var pagingQuery = $@"SELECT inv.Id AS InventoryId, s.*
                                  FROM Stores s LEFT JOIN Inventories inv ON s.Id = inv.StoreId
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                 FROM Stores s
                                 WHERE {filterClause}";

                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);
                var stores = await connection.QueryAsync<StoreReturnModel, AddressReturnModel, StoreReturnModel>(pagingQuery, (s, a) =>
                    {
                        s.Address = a;
                        return s;
                    },
                    splitOn: "Address_Street",
                    param: new { rq.PageSize, rq.PageNumber });

                return new PagedList<StoreReturnModel> { Data = stores, RowsCount = rowsCount };
            }
        }

        private string BuildGetStoresFilterClause(ListRequestModel rq, string multipartIdentifier = "")
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({multipartIdentifier}.{nameof(Store.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {multipartIdentifier}.{nameof(Store.Hotline)} LIKE '%{rq.SearchTerm}%' 
                                        OR {multipartIdentifier}.Address_City LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_Country LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_District LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_State LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_Street LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_ZipCode LIKE '%{rq.SearchTerm}%'
                                        OR {multipartIdentifier}.Address_Ward LIKE '%{rq.SearchTerm}%')";
            }

            // non-deleted clause
            var notDeletedClause = $"{multipartIdentifier}.IsDeleted = 0";

            // final clause
            var filterClause = $"{notDeletedClause} AND {searchTermClause}";

            return filterClause;
        }

        private string BuildGetStoresOrderByClause(ListRequestModel rq, string multipartIdentifier = "")
        {
            var orderByClause = $"{multipartIdentifier}.CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
