﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooHoo.WebApp.Application.Queries.RequestModels
{
    public class ListRequestModel
    {
        public string SearchTerm { get; set; }

        public string SortData { get; set; }

        public int PageNumber { get; set; } = 1;

        public int PageSize { get; set; } = 10;
    }
}
