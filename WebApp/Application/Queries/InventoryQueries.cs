﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WooHoo.Domain.Aggregates.InventoryAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;
using WooHoo.WebApp.Application.Queries.ReturnModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IInventoryQueries
    {
        Task<InventoryReturnModel> GetInventoryAsync(int id);
        Task<PagedList<InventoryReturnModel>> GetInventoriesAsync(ListRequestModel rq);
        Task<List<DropDownModel>> GetInventoryDropdownListAsync();
        Task<List<MaterialInventoryItemReturnModel>> GetMaterialItems(int inventoryId);
    }

    public class InventoryQueries : IInventoryQueries
    {
        private readonly string _connectionString;

        public InventoryQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<InventoryReturnModel> GetInventoryAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT inv.*, store.[Name] as StoreName
                              FROM Inventories inv 
                                LEFT JOIN Stores store ON inv.StoreId = store.Id
                              WHERE inv.Id = @id AND inv.IsDeleted = 0";

                var inventory = await connection.QueryFirstOrDefaultAsync<InventoryReturnModel>(query, new { id });

                return inventory;
            }
        }

        public async Task<PagedList<InventoryReturnModel>> GetInventoriesAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetInventoriesFilterClause(rq, "inv");
                var orderByClause = BuildGetInventoriesOrderByClause(rq, "inv");

                var pagingQuery = $@"SELECT inv.*, store.[Name] as StoreName
                                  FROM Inventories inv LEFT JOIN Stores store ON inv.StoreId = store.Id
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                  FROM Inventories
                                  WHERE {filterClause}";

                var inventories = await connection.QueryAsync<InventoryReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<InventoryReturnModel> { Data = inventories, RowsCount = rowsCount };
            }
        }

        public async Task<List<DropDownModel>> GetInventoryDropdownListAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT Id as Value, Name as Label
                              FROM Inventories 
                              WHERE IsDeleted = 0
                              ORDER BY Name";


                var inventories = await connection.QueryAsync<DropDownModel>(query);

                return inventories.ToList();
            }
        }

        public async Task<List<MaterialInventoryItemReturnModel>> GetMaterialItems(int inventoryId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT mi.*, m.Name as MaterialName, m.Id as MaterialId, s.Name as SupplierName, s.Id as SupplierId
                            FROM MaterialInventoryItems mi
                                 JOIN Materials m ON m.Id = mi.MaterialId
	                             LEFT JOIN Suppliers s ON s.Id = mi.SupplierId
                            ORDER BY m.Name, mi.ReceiptDate, mi.ConsignmentCode";


                var items = await connection.QueryAsync<MaterialInventoryItemReturnModel>(query);

                return items.ToList();
            }
        }

        private string BuildGetInventoriesFilterClause(ListRequestModel rq, string multipartIdentifier = "")
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({multipartIdentifier}.{nameof(Inventory.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {multipartIdentifier}.{nameof(Inventory.Address)} LIKE '%{rq.SearchTerm}%')";
            }

            // non-deleted clause
            var notDeletedClause = $"{multipartIdentifier}.IsDeleted = 0";

            // final clause
            var filterClause = $"{notDeletedClause} AND {searchTermClause}";

            return filterClause;
        }

        private string BuildGetInventoriesOrderByClause(ListRequestModel rq, string multipartIdentifier = "")
        {
            var orderByClause = $"{multipartIdentifier}.CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
