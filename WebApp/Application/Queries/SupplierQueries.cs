﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using WooHoo.WebApp.Application.Queries.ReturnModels;
using Microsoft.Extensions.Configuration;
using WooHoo.Domain.Aggregates.SupplierAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface ISupplierQueries
    {
        Task<SupplierReturnModel> GetSupplierAsync(int id);
        Task<PagedList<SupplierReturnModel>> GetSuppliersAsync(ListRequestModel rq);
        Task<List<DropDownModel>> GetSupplierDropdownListAsync();
    }

    public class SupplierQueries : ISupplierQueries
    {
        private readonly string _connectionString;

        public SupplierQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<SupplierReturnModel> GetSupplierAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT *
                              FROM Suppliers
                              WHERE Id = @id";

                var supplier = await connection.QueryFirstOrDefaultAsync<SupplierReturnModel>(query, new { id });

                return supplier;
            }
        }

        public async Task<PagedList<SupplierReturnModel>> GetSuppliersAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetSuppliersFilterClause(rq);
                var orderByClause = BuildGetSuppliersOrderByClause(rq);

                var pagingQuery = $@"SELECT *
                                  FROM Suppliers
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                  FROM Suppliers
                                  WHERE {filterClause}";

                var suppliers = await connection.QueryAsync<SupplierReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<SupplierReturnModel> { Data = suppliers, RowsCount = rowsCount };
            }
        }

        public async Task<List<DropDownModel>> GetSupplierDropdownListAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT Id as Value, Name as Label
                              FROM Suppliers 
                              WHERE IsDeleted = 0
                              ORDER BY Name";


                var suppliers = await connection.QueryAsync<DropDownModel>(query);

                return suppliers.ToList();
            }
        }

        private string BuildGetSuppliersFilterClause(ListRequestModel rq)
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({nameof(Supplier.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {nameof(Supplier.Description)} LIKE '%{rq.SearchTerm}%')";
            }

            // non-deleted clause
            var notDeletedClause = "IsDeleted = 0";

            // final clause
            var filterClause = $"{notDeletedClause} AND {searchTermClause}";

            return filterClause;
        }

        private string BuildGetSuppliersOrderByClause(ListRequestModel rq)
        {
            var orderByClause = "CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
