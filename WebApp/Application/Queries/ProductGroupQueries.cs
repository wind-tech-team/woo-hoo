﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using WooHoo.WebApp.Application.Queries.ReturnModels;
using Microsoft.Extensions.Configuration;
using WooHoo.Domain.Aggregates.ProductGroupAggregate;
using WooHoo.WebApp.Application.Queries.RequestModels;

namespace WooHoo.WebApp.Application.Queries
{
    public interface IProductGroupQueries
    {
        Task<ProductGroupReturnModel> GetProductGroupAsync(int id);
        Task<PagedList<ProductGroupReturnModel>> GetProductGroupsAsync(ListRequestModel rq);
    }

    public class ProductGroupQueries : IProductGroupQueries
    {
        private readonly string _connectionString;

        public ProductGroupQueries(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<ProductGroupReturnModel> GetProductGroupAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"SELECT *
                              FROM ProductGroups
                              WHERE Id = @id";

                var productGroup = await connection.QueryFirstOrDefaultAsync<ProductGroupReturnModel>(query, new { id });

                return productGroup;
            }
        }

        public async Task<PagedList<ProductGroupReturnModel>> GetProductGroupsAsync(ListRequestModel rq)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var filterClause = BuildGetProductGroupsFilterClause(rq);
                var orderByClause = BuildGetProductGroupsOrderByClause(rq);

                var pagingQuery = $@"SELECT *
                                  FROM ProductGroups
                                  WHERE {filterClause}
                                  ORDER BY {orderByClause} 
                                  OFFSET @PageSize * (@PageNumber - 1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY;";

                var countQuery = $@"SELECT COUNT(*)
                                  FROM ProductGroups
                                  WHERE {filterClause}";

                var productGroups = await connection.QueryAsync<ProductGroupReturnModel>(pagingQuery, new { rq.PageSize, rq.PageNumber });
                var rowsCount = await connection.QuerySingleAsync<int>(countQuery);

                return new PagedList<ProductGroupReturnModel> { Data = productGroups, RowsCount = rowsCount };
            }
        }

        private string BuildGetProductGroupsFilterClause(ListRequestModel rq)
        {
            // search clause
            var searchTermClause = "1 = 1";
            if (!string.IsNullOrEmpty(searchTermClause))
            {
                searchTermClause = $@"({nameof(ProductGroup.Name)} LIKE '%{rq.SearchTerm}%' 
                                        OR {nameof(ProductGroup.Description)} LIKE '%{rq.SearchTerm}%')";
            }

            // final clause
            var filterClause = $"{searchTermClause}";

            return filterClause;
        }

        private string BuildGetProductGroupsOrderByClause(ListRequestModel rq)
        {
            var orderByClause = "CreatedDate DESC";
            if (!string.IsNullOrEmpty(rq.SortData))
            {
                orderByClause = rq.SortData;
            }

            return orderByClause;
        }
    }
}
