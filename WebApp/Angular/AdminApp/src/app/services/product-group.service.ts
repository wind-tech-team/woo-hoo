import { PagedList, ListRequest } from './../models/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductGroup } from 'app/models/product-group.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class ProductGroupService extends CrudService<ProductGroup> {
    apiEndpoint: string = '/api/ProductGroups';
}
