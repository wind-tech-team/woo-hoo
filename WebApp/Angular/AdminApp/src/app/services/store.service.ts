import { PagedList, ListRequest } from './../models/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from 'app/models/store.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class StoreService extends CrudService<Store> {
    apiEndpoint: string = '/api/Stores';
}
