import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MaterialGroup } from 'app/models/material-group.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class MaterialGroupService extends CrudService<MaterialGroup> {
    apiEndpoint: string = '/api/MaterialGroups';

    getDropdownList(): Observable<MaterialGroup[]> {
        return this.httpClient.get<MaterialGroup[]>(`${this.apiEndpoint}/DropdownList`);
    }
}

