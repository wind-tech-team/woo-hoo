import { PagedList, ListRequest } from '../models/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Material } from 'app/models/material.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class MaterialService extends CrudService<Material> {
    apiEndpoint: string = '/api/Materials';

    calculateAverageCostPerSaleUnit(averagePricePerInventoryUnit: number, inventoryToSaleAmountPerUnit: number): Number {
        if (Number.isNaN(averagePricePerInventoryUnit) || Number.isNaN(inventoryToSaleAmountPerUnit) || inventoryToSaleAmountPerUnit === 0) {
            return null;
        }

        return +((averagePricePerInventoryUnit / inventoryToSaleAmountPerUnit).toFixed(2));
    }

    getDropdownList(): Observable<Material[]> {
        return this.httpClient.get<Material[]>(`${this.apiEndpoint}/DropdownList`);
    }
}

