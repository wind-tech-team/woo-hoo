import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Supplier } from 'app/models/supplier.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class SupplierService extends CrudService<Supplier> {
    apiEndpoint: string = '/api/Suppliers';

    getDropdownList(): Observable<Supplier[]> {
        return this.httpClient.get<Supplier[]>(`${this.apiEndpoint}/DropdownList`);
    }
}
