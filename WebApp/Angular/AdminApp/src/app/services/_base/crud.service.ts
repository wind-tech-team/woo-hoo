import { CrudModel } from './../../models/_base/crud.model';
import { ListRequest, PagedList } from "app/models";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

export interface ICrudService<T> {
    list(rq?: ListRequest): Observable<PagedList<T>>;
    getById(id: number): Observable<T>
    create(data: T): Observable<T>;
    update(data: T): Observable<T>;
    delete(data: T): Observable<any>;
    deleteMany(data: T[]): Observable<any>
}

@Injectable()
export abstract class CrudService<T extends CrudModel> implements ICrudService<T> {

    abstract apiEndpoint: string;

    constructor(protected httpClient: HttpClient) {
    }

    list(rq: ListRequest = new ListRequest()): Observable<PagedList<T>> {
        let options = {
            params: {
                searchTerm: rq.searchTerm,
                sortData: rq.sortData,
                pageNumber: rq.pageNumber.toString(),
                pageSize: rq.pageSize.toString()
            }
        };

        return this.httpClient.get<PagedList<T>>(this.apiEndpoint, options);
    }

    getById(id: number): Observable<T> {
        return this.httpClient.get<T>(`${this.apiEndpoint}/${id}`);
    }

    create(data: T): Observable<T> {
        return this.httpClient.post<T>(this.apiEndpoint, data);
    }

    update(data: T): Observable<T> {
        return this.httpClient.put<T>(this.apiEndpoint, data);
    }

    delete(data: T): Observable<any> {
        let options = {
            params: {
                rowVersionString: data.rowVersionString
            }
        };

        return this.httpClient.delete(`${this.apiEndpoint}/${data.id}`, options);
    }

    deleteMany(data: T[]): Observable<any> {
        let body = {
            items: data.map(x => { return { id: x.id, rowVersionString: x.rowVersionString } })
        };

        return this.httpClient.request('delete', this.apiEndpoint, { body });
    }
}