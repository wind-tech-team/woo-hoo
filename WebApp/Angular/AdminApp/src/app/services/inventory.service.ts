import { PagedList, ListRequest } from './../models/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Inventory, InventoryMaterialInputModel } from 'app/models/inventory.model';
import { CrudService } from './_base/crud.service';

@Injectable()
export class InventoryService extends CrudService<Inventory> {
    apiEndpoint: string = '/api/Inventories';

    getDropdownList(): Observable<Inventory[]> {
        return this.httpClient.get<Inventory[]>(`${this.apiEndpoint}/DropdownList`);
    }

    inputMaterials(model: InventoryMaterialInputModel): Observable<any> {
        return this.httpClient.post<any>(`${this.apiEndpoint}/InputMaterials`, model);
    }
}
