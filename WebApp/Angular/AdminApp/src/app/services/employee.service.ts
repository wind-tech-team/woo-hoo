
import { Injectable } from '@angular/core';
import { CrudService } from './_base/crud.service';
import {Employee} from '../models/employee.model';

@Injectable()
export class EmployeeService extends CrudService<Employee> {
    apiEndpoint: string = '/api/Employees';
}
