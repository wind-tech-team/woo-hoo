import { MatDialogModule, MatListModule, MatButtonModule, MatFormFieldModule, 
  MatIconModule, MatInputModule, MatSelectModule, MatStepperModule, MatCardModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ConfirmationDialogComponent, MediaUploadDialogComponent, AlertDialogComponent, PickerDialogComponent } from './dialog/index';
import { SafeHtmlDirective } from './safe-html.directive';
import { AddressComponent } from './address/address.component';
import { TranslateModule } from '@ngx-translate/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    FormsModule,

    MatDialogModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatCardModule,
    ReactiveFormsModule,
    TranslateModule,
    GooglePlaceModule
  ],
  declarations: [
    SafeHtmlDirective,
    ConfirmationDialogComponent,
    AlertDialogComponent,
    PickerDialogComponent,
    AddressComponent
    // MediaUploadDialogComponent
  ],
  exports: [
    SafeHtmlDirective,
    AddressComponent
  ],
  providers: [
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    AlertDialogComponent,
    // MediaUploadDialogComponent,
    PickerDialogComponent
  ],
})

export class AppSharedModule { }
