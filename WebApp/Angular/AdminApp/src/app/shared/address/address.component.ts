import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from '../_i18n/en';
import { locale as vietnam } from '../_i18n/en';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Address as AddressModel } from '../../models/address.model';
import { from } from 'rxjs';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  form: FormGroup;

  @Input() addressModel: AddressModel;
  @Output() addressModelChanged: EventEmitter<AddressModel> = new EventEmitter();

  constructor(private _formBuilder: FormBuilder, private fuseTranslationLoaderService: FuseTranslationLoaderService) {
    this.fuseTranslationLoaderService.loadTranslations(english, vietnam);
  }

  ngOnInit() {

    if (!this.addressModel) {
      this.addressModel = new AddressModel();
    }

    this.form = this._formBuilder.group({
      street: [this.addressModel.street, Validators.required],
      addressLine: [this.addressModel.addressLine, Validators.required],
      ward: [this.addressModel.ward, Validators.required],
      district: [this.addressModel.district, Validators.required],
      city: [this.addressModel.city, Validators.required],
      state: [this.addressModel.state],
      country: [this.addressModel.country, Validators.required],
      zipCode: [this.addressModel.zipCode, [Validators.maxLength(5)]]
    });
  }

  public handleAddressChange(address: Address) {
    this.addressModel = this.convertToAddressModel(address);
    this.form.get('street').setValue(this.addressModel.street);
    this.form.get('addressLine').setValue(this.addressModel.addressLine);
    this.form.get('ward').setValue(this.addressModel.ward);
    this.form.get('district').setValue(this.addressModel.district);
    this.form.get('city').setValue(this.addressModel.city);
    this.form.get('state').setValue(this.addressModel.state);
    this.form.get('country').setValue(this.addressModel.country);
    this.form.get('zipCode').setValue(this.addressModel.zipCode);

    this.addressModelChanged.emit(this.addressModel);
  }

  private convertToAddressModel(address: Address): AddressModel {
    var result = new AddressModel();
    result.addressLine = address.formatted_address;
    result.street = '';

    address.address_components.forEach(function (item) {
      if (item['types'].includes("administrative_area_level_1")) {
        result.city = item['short_name'];
      } else if (item['types'].includes("street_number")) {
        result.street += item['long_name'];
      } else if (item['types'].includes("route")) {
        result.street += " " + item['long_name'];
      } else if (item['types'].includes("administrative_area_level_2")) {
        result.district = item['short_name'];
      } else if (item['types'].includes("locality")) {
        result.state = item['long_name'];
      } else if (item['types'].includes("country")) {
        result.country = item['long_name'];
      } else if (item['types'].includes("postal_code")) {
        result.zipCode = item['short_name'];
      }
    });

    result.ward = this.getWardFromAddress(result);

    return result;
  }

  private getWardFromAddress(address: AddressModel): string {
    var ward: string = '';
    var addressItems = address.addressLine.split(",", 2);
    addressItems.forEach(function (item) {
      if (!item.includes(address.street)) { ward = item };
    });

    return ward;
  }

}
