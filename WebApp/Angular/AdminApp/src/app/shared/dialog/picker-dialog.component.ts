import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'picker-dialog',
  templateUrl: './picker-dialog.component.html'
})
export class PickerDialogComponent {

  public selectedValues: any[] = [];

  constructor(public dialogRef: MatDialogRef<PickerDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
