import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { AlertDialogComponent } from './alert-dialog.component';
import { MediaUploadDialogComponent } from './media-upload-dialog.component';
import { PickerDialogComponent } from './picker-dialog.component';

@Injectable()
export class DialogService {
  constructor(public dialog: MatDialog) { }

  showAlert(title: string, content: string, isHtml: boolean = false, callback: (dialogResult: boolean) => void = null): void {
    let dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: { title: title, content: content, isHtml: isHtml }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (callback) {
        callback(result);
      }
    });
  }

  showConfirmation(title: string, content: string, callback: (dialogResult: boolean) => void = null): void {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: { title: title, content: content }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (callback) {
        callback(result);
      }
    });
  }

  showMediaUpload(title: string, callback: (dialogResult: any) => void = null): void {
    let dialogRef = this.dialog.open(MediaUploadDialogComponent, {
      width: '800px',
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (callback) {
        callback(result);
      }
    });
  }

  showPicker(title: string, items: any[], valueField: string, textField: string, callback: (dialogResult: any) => void = null): void {
    let dialogRef = this.dialog.open(PickerDialogComponent, {
      width: '400px',
      data: { title: title, items: items, valueField: valueField, textField: textField }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (callback) {
        callback(result);
      }
    });
  }
}
