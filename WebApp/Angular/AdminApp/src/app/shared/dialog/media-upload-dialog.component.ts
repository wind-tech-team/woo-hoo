import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'media-upload-dialog',
  templateUrl: './media-upload-dialog.component.html'
})
export class MediaUploadDialogComponent {

  public uploadedMedia: any[] = [];

  constructor(public dialogRef: MatDialogRef<MediaUploadDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }

  onMediaUploaded(event) {
    this.uploadedMedia.push(event);
  }
}
