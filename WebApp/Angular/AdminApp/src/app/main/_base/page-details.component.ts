import { ICrudService } from './../../services/_base/crud.service';
import { CrudModel } from './../../models/_base/crud.model';
import { OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { AppUtil } from './app-util';
import { Observable } from 'rxjs';

export abstract class PageDetailsComponent<T extends CrudModel> implements OnInit {

  public isEditMode: boolean;
  public isCreateMode: boolean;

  protected resolvedData: any;

  protected abstract getDataForSubmit(): T;

  constructor(protected fb: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, protected crudService: ICrudService<T>) {
    this.resolvedData = this.route.snapshot.data.pageData;
  }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get("id"));
    if (id === 0) {
      this.isCreateMode = true;
    } else {
      this.isEditMode = true;
    }
  }

  submit(data: T): Observable<T> {
    if (data.id === 0) {
      return this.crudService.create(data);
    } else {
      return this.crudService.update(data);
    }
  }

  async save() {
    const data = await this.submit(this.getDataForSubmit()).toPromise();

    if (this.isCreateMode) {
      const successMsg = await this.appUtil.translate.get('BASE.CREATE_SUCCESS').toPromise();
      this.appUtil.snackBar.open(successMsg);
    } else {
      const successMsg = await this.appUtil.translate.get('BASE.UPDATE_SUCCESS').toPromise();
      this.appUtil.snackBar.open(successMsg);
    }

    this.appUtil.router.navigate([`..`], { relativeTo: this.route });
  }

  async delete() {
    const confirmationTitle = await this.appUtil.translate.get('BASE.DELETE_COMFIRMATION_TITLE').toPromise();
    const confirmationMsg = await this.appUtil.translate.get('BASE.DELETE_CONFIRMATION_MESSAGE_NO_PARAMS').toPromise();

    this.appUtil.dialog.showConfirmation(confirmationTitle, confirmationMsg, async (dlgResult) => {
      if (dlgResult) {
        const data = this.getDataForSubmit();
        await this.crudService.delete(data).toPromise();

        const successMsg = await this.appUtil.translate.get('BASE.DELETE_SUCCESS').toPromise();
        this.appUtil.snackBar.open(successMsg);

        this.appUtil.router.navigate([`..`], { relativeTo: this.route });
      }
    });
  }
}
