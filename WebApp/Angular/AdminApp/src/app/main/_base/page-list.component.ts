import { ICrudService } from './../../services/_base/crud.service';
import { CrudModel } from './../../models/_base/crud.model';
import { ViewChild, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort } from '@angular/material';
import { merge, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

import { MatTableServerDataSource } from './mat-table-server-data-source';
import { ListRequest } from 'app/models';
import { AppUtil } from './app-util';

export abstract class PageListComponent<T extends CrudModel> implements OnInit {

  dataSource: MatTableServerDataSource<T>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') inputSearch: ElementRef;

  normalizedRows: { [id: number]: T };
  selectedRowIds: number[] = [];

  get hasSelectedRows() {
    return this.selectedRowIds.length > 0;
  }

  constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected crudService: ICrudService<T>) {
  }

  ngOnInit() {
    this.dataSource = new MatTableServerDataSource<T>();
    this.dataSource.loadData(async () => await this.crudService.list().toPromise());
    this.dataSource.data$.subscribe(data => {
      this.normalizedRows = data.reduce(
        (_rows: { [id: number]: T }, row: T) => {
          return {
            ..._rows,
            [row.id]: row
          };
        }, {});

      this.selectedRowIds = data.filter(x => this.selectedRowIds.indexOf(x.id) > -1).map(x => x.id);
    });
  }

  ngAfterViewInit() {
    const filterChanged$ = fromEvent(this.inputSearch.nativeElement, 'keyup').pipe(
      debounceTime(500), // don't fire requests on every keys, 500ms delay
      distinctUntilChanged() // don't fire request for the same input value
    );

    const sortChanged$ = this.sort.sortChange;
    const pageChanged$ = this.paginator.page;

    merge(filterChanged$, sortChanged$).subscribe(() => this.paginator.pageIndex = 0);
    merge(filterChanged$, sortChanged$, pageChanged$).subscribe(() => {
      let rq = new ListRequest();
      rq.searchTerm = this.getSearchTerm();
      rq.sortData = this.getSortExpression();
      rq.pageNumber = this.paginator.pageIndex + 1;
      rq.pageSize = this.paginator.pageSize;

      this.dataSource.loadData(async () => await this.crudService.list(rq).toPromise());
    });
  }

  isRowSelected(rowId: number) {
    return this.selectedRowIds.indexOf(rowId) > -1;
  }

  onRowCheckboxChanged(id: number) {
    if (this.selectedRowIds.find(x => x === id)) {
      this.selectedRowIds = this.selectedRowIds.filter(x => x !== id);
    }
    else {
      this.selectedRowIds.push(id);
    }
  }

  async deleteSelectedRows() {
    const confirmationTitle = await this.appUtil.translate.get('BASE.DELETE_COMFIRMATION_TITLE').toPromise();
    const confirmationMsg = await this.appUtil.translate.get('BASE.DELETE_SELECTED_CONFIRMATION_MESSAGE').toPromise();

    this.appUtil.dialog.showConfirmation(confirmationTitle, confirmationMsg, async (dlgResult) => {
      if (dlgResult) {
        // delete selected rows
        const rowsToDelete = this.selectedRowIds.map(x => this.normalizedRows[x]);
        await this.crudService.deleteMany(rowsToDelete).toPromise();

        const successMsg = await this.appUtil.translate.get('BASE.DELETE_SUCCESS').toPromise();
        this.appUtil.snackBar.open(successMsg);

        // reload data
        let rq = new ListRequest();
        rq.searchTerm = this.getSearchTerm();
        rq.sortData = this.getSortExpression();

        this.dataSource.loadData(async () => await this.crudService.list(rq).toPromise());
      }
    });
  }

  async onRowDeleteClicked($event, row: T, name: string) {
    $event.stopPropagation();

    const confirmationTitle = await this.appUtil.translate.get('BASE.DELETE_COMFIRMATION_TITLE').toPromise();
    const confirmationMsg = await this.appUtil.translate.get('BASE.DELETE_CONFIRMATION_MESSAGE', { name }).toPromise();

    this.appUtil.dialog.showConfirmation(confirmationTitle, confirmationMsg, async (dlgResult) => {
      if (dlgResult) {
        await this.crudService.delete(row).toPromise();

        const successMsg = await this.appUtil.translate.get('BASE.DELETE_SUCCESS').toPromise();
        this.appUtil.snackBar.open(successMsg);

        // reload data
        let rq = new ListRequest();
        rq.searchTerm = this.getSearchTerm();
        rq.sortData = this.getSortExpression();

        this.dataSource.loadData(async () => await this.crudService.list(rq).toPromise());
      }
    });
  }

  private getSearchTerm(): string {
    return this.inputSearch ? this.inputSearch.nativeElement.value : '';
  }

  private getSortExpression(): string {
    return this.sort.active && this.sort.direction ? `${this.sort.active} ${this.sort.direction}` : '';
  }
}
