import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { DialogService } from '../../shared/dialog/index';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AppUtil {
  constructor(public snackBar: MatSnackBar, public dialog: DialogService, public translate: TranslateService, public router: Router) { }
}
