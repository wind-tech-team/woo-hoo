import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject } from "rxjs";

export class MatTableServerDataSource<T> implements DataSource<T> {

  private dataSubject = new BehaviorSubject<T[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private dataLengthSubject = new BehaviorSubject<number>(0);

  data$ = this.dataSubject.asObservable();
  loading$ = this.loadingSubject.asObservable();
  dataLength$ = this.dataLengthSubject.asObservable();

  connect(collectionViewer: CollectionViewer): Observable<T[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
    this.loadingSubject.complete();
  }

  async loadData(func: Function) {
    try {
      this.loadingSubject.next(true);
      let rs = await func();
      this.dataLengthSubject.next(rs.rowsCount);
      this.dataSubject.next(rs.data);
    } finally {
      this.loadingSubject.next(false);
    }

    this.loadingSubject.next(true);
  }
}
