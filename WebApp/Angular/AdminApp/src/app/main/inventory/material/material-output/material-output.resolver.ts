import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { InventoryService, MaterialService } from 'app/services';

@Injectable()
export class MaterialOutputResolver implements Resolve<any> {
    constructor(private inventoryService: InventoryService, private materialService: MaterialService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
        const inventories = await this.inventoryService.getDropdownList().toPromise();
        const materials = await this.materialService.getDropdownList().toPromise();

        return { dropdowns: { inventories, materials } };
    }
}
