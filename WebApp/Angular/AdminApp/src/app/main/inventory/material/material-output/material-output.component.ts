import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppUtil } from 'app/main/_base';
import { InventoryService } from 'app/services';

import * as moment from 'moment';

@Component({
    selector: 'app-material-output',
    templateUrl: './material-output.component.html',
    // styleUrls: ['./material-output.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class MaterialOutputComponent implements OnInit {
    issueForm: FormGroup;
    dropdowns: any;

    constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private inventoryService: InventoryService) {
        const resolvedData = this.route.snapshot.data.pageData;

        this.issueForm = this.buildIssueForm();

        this.dropdowns = resolvedData.dropdowns;
    }

    ngOnInit(): void {
    }

    private buildIssueForm(): FormGroup {
        return this.formBuilder.group({
            inventoryId: ['', Validators.required],
            issueDate: [moment(), Validators.required],
            note: ['']
        });
    }
}
