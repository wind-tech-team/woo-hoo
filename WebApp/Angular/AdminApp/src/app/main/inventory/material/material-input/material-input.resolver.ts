import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { InventoryService, SupplierService, MaterialService } from 'app/services';

@Injectable()
export class MaterialInputResolver implements Resolve<any> {
    constructor(private inventoryService: InventoryService, private supplierService: SupplierService, private materialService: MaterialService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
        const inventories = await this.inventoryService.getDropdownList().toPromise();
        const suppliers = await this.supplierService.getDropdownList().toPromise();
        const materials = await this.materialService.getDropdownList().toPromise();

        return { dropdowns: { inventories, suppliers, materials } };
    }
}
