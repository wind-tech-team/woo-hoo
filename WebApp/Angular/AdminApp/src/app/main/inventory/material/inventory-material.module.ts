import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS, MatDatepickerModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { MaterialInputComponent } from './material-input/material-input.component';
import { MaterialOutputComponent } from './material-output/material-output.component';
import { MaterialInputResolver } from './material-input/material-input.resolver';
import { SupplierService, MaterialService, InventoryService } from 'app/services';

const routes: Routes = [
    {
        path: 'input',
        component: MaterialInputComponent,
        resolve: { pageData: MaterialInputResolver }
    },
    {
        path: 'output',
        component: MaterialOutputComponent
    }
];

@NgModule({
    declarations: [
        MaterialInputComponent,
        MaterialOutputComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,
        MatDatepickerModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        InventoryService,
        SupplierService,
        MaterialService,
        MaterialInputResolver
    ],
})
export class InventoryMaterialModule {

}