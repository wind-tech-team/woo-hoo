import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppUtil } from 'app/main/_base';

import * as moment from 'moment';
import { InventoryMaterialInputModel, InventoryMaterialInputItemModel } from 'app/models';
import { InventoryService } from 'app/services';

@Component({
    selector: 'app-material-input',
    templateUrl: './material-input.component.html',
    styleUrls: ['./material-input.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class MaterialInputComponent implements OnInit {
    displayedColumns = ['material', 'supplier', 'consignmentCode', 'quantity', 'actions'];
    receiptForm: FormGroup;
    itemForm: FormGroup;
    dropdowns: any;
    inventoryUnit: string;
    itemsDataSource: any[] = [];

    constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private inventoryService: InventoryService) {
        const resolvedData = this.route.snapshot.data.pageData;

        this.receiptForm = this.buildReceiptForm();
        this.itemForm = this.buildItemForm();

        this.dropdowns = resolvedData.dropdowns;
    }

    ngOnInit(): void {
        this.setItemInventoryUnitOnMaterialSelectionChanged();
    }

    public onAddItemClicked() {
        const item = this.itemForm.getRawValue();
        const selectedMaterial = this.dropdowns.materials.filter(x => x.value === item.materialId)[0];
        if (selectedMaterial) {
            item.materialDisplay = selectedMaterial.label;
        }

        const selectedSupplier = this.dropdowns.suppliers.filter(x => x.value === item.supplierId)[0];
        if (selectedSupplier) {
            item.supplierDisplay = selectedSupplier.label;
        }

        item.quantityDisplay = `${item.quantity} ${this.inventoryUnit}`;

        this.itemsDataSource.push(item);
        this.refreshItemsDataSource();
    }

    public onItemRowDeleteClicked($event, rowIndex) {
        $event.stopPropagation();

        this.itemsDataSource.splice(rowIndex, 1);
        this.refreshItemsDataSource();
    }

    public async save() {
        const receipt = this.receiptForm.getRawValue();
        let inputModel = new InventoryMaterialInputModel(receipt.inventoryId, receipt.receiptDate, receipt.price, receipt.note);

        this.itemsDataSource.forEach(item => {
            let inputItemModel = new InventoryMaterialInputItemModel(item.materialId, item.quantity, item.supplierId, item.consignmentCode);
            inputModel.items.push(inputItemModel);
        });

        await this.inventoryService.inputMaterials(inputModel).toPromise();

        const successMsg = await this.appUtil.translate.get('INVENTORY_MANAGEMENT.MATERIAL_INPUT.INPUT_SUCCESS').toPromise();
        this.appUtil.snackBar.open(successMsg);

        this.appUtil.router.navigate([`/sample`]);
    }

    private buildReceiptForm(): FormGroup {
        return this.formBuilder.group({
            inventoryId: ['', Validators.required],
            receiptDate: [moment(), Validators.required],
            note: [''],
            price: ['']
        });
    }

    private buildItemForm(): FormGroup {
        return this.formBuilder.group({
            materialId: ['', Validators.required],
            supplierId: [''],
            quantity: ['', Validators.required],
            consignmentCode: ['']
        });
    }

    private setItemInventoryUnitOnMaterialSelectionChanged(): void {
        this.itemForm.get('materialId').valueChanges.subscribe(() => {
            const selectedMaterialId = this.itemForm.get('materialId').value;
            const selectedMaterial = this.dropdowns.materials.filter(x => x.value === selectedMaterialId)[0];
            this.inventoryUnit = selectedMaterial.tag;
        });
    }

    private refreshItemsDataSource(): void {
        this.itemsDataSource = [...this.itemsDataSource];
    }
}
