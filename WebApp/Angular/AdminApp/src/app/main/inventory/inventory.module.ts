import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes: Routes = [
    {
        path: 'materials',
        loadChildren: './material/inventory-material.module#InventoryMaterialModule'
    },
    // {
    //     path: 'products',
    //     loadChildren: './product/product.module#ProductModule'
    // }
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    exports: [],
    providers: [],
})
export class InventoryModule { }