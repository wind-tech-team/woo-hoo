import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { ProductGroupService } from 'app/services/index';
import { ProductGroup } from 'app/models';
import { Observable, of } from 'rxjs';

@Injectable()
export class ProductGroupDetailsResolver implements Resolve<ProductGroup> {
    constructor(private productGroupService: ProductGroupService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<ProductGroup> {
        let id = Number(route.paramMap.get('id'));
        if (id === 0) {
            return of(new ProductGroup());
        }

        return this.productGroupService.getById(Number(id));
    }
}
