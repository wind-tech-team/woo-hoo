import { ProductGroup } from 'app/models';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';
import { ProductGroupService } from 'app/services';

@Component({
	selector: 'app-product-group-details',
	templateUrl: './product-group-details.component.html',
	styleUrls: ['./product-group-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class ProductGroupDetailsComponent extends PageDetailsComponent<ProductGroup> {

	form: FormGroup;
	productGroup: ProductGroup;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private productGroupService: ProductGroupService) {
		super(formBuilder, route, appUtil, productGroupService);

		this.productGroup = this.resolvedData as ProductGroup;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): ProductGroup {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.productGroup.id],
			name: [this.productGroup.name, Validators.required],
			description: [this.productGroup.description],
			rowVersionString: [this.productGroup.rowVersionString]
		});
	}
}
