﻿import { ProductGroupService } from '../../../services/product-group.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductGroupListComponent } from './product-group-list/product-group-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { ProductGroupDetailsComponent } from './product-group-details/product-group-details.component';
import { ProductGroupDetailsResolver } from './product-group-details/product-group-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: ProductGroupListComponent
    },
    {
        path: ':id',
        component: ProductGroupDetailsComponent,
        resolve: { pageData: ProductGroupDetailsResolver }
    }
];

@NgModule({
    declarations: [
        ProductGroupListComponent,
        ProductGroupDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        ProductGroupService,
        ProductGroupDetailsResolver
    ],
})
export class MasterDataProductGroupModule {

}