import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { ProductGroupService } from 'app/services';
import { ProductGroup } from 'app/models';

@Component({
	selector: 'app-product-group-list',
	templateUrl: './product-group-list.component.html',
	styleUrls: ['./product-group-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class ProductGroupListComponent extends PageListComponent<ProductGroup> {
	displayedColumns = ['select', 'id', 'name', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected productGroupService: ProductGroupService) {
		super(route, appUtil, productGroupService);
	}
}
