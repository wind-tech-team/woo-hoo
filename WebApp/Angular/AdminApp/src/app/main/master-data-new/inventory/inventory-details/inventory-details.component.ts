import { InventoryService } from './../../../../services/inventory.service';
import { Inventory } from 'app/models/inventory.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';

@Component({
	selector: 'app-inventory-details',
	templateUrl: './inventory-details.component.html',
	styleUrls: ['./inventory-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class InventoryDetailsComponent extends PageDetailsComponent<Inventory> {

	form: FormGroup;
	inventory: Inventory;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private inventoryService: InventoryService) {
		super(formBuilder, route, appUtil, inventoryService);

		this.inventory = this.resolvedData as Inventory;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): Inventory {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.inventory.id],
			name: [this.inventory.name, Validators.required],
			storeName: [this.inventory.storeName],
			storeId: [this.inventory.storeId],
			address: [this.inventory.address],
			rowVersionString: [this.inventory.rowVersionString]
		});
	}
}
