import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { InventoryService } from 'app/services/inventory.service';
import { Inventory } from 'app/models';

@Component({
	selector: 'app-inventory-list',
	templateUrl: './inventory-list.component.html',
	styleUrls: ['./inventory-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class InventoryListComponent extends PageListComponent<Inventory> {
	displayedColumns = ['select', 'id', 'name', 'address', 'storeName', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected inventoryService: InventoryService) {
		super(route, appUtil, inventoryService);
	}
}
