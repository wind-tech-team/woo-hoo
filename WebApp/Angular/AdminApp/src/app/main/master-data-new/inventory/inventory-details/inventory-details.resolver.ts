import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { InventoryService } from 'app/services/index';
import { Inventory } from 'app/models';
import { Observable, of } from 'rxjs';

@Injectable()
export class InventoryDetailsResolver implements Resolve<Inventory> {
    constructor(private inventoryService: InventoryService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Inventory> {
        let id = Number(route.paramMap.get('id'));
        if (id === 0) {
            return of(new Inventory());
        }

        return this.inventoryService.getById(Number(id));
    }
}
