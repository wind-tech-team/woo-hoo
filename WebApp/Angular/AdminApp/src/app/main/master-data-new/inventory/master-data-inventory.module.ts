﻿import { InventoryService } from '../../../services/inventory.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { InventoryDetailsComponent } from './inventory-details/inventory-details.component';
import { InventoryDetailsResolver } from './inventory-details/inventory-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: InventoryListComponent
    },
    {
        path: ':id',
        component: InventoryDetailsComponent,
        resolve: { pageData: InventoryDetailsResolver }
    }
];

@NgModule({
    declarations: [
        InventoryListComponent,
        InventoryDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        InventoryService,
        InventoryDetailsResolver
    ],
})
export class MasterDataInventoryModule {

}