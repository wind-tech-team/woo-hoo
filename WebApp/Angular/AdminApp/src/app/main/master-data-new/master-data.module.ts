import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes: Routes = [
    {
        path: 'stores',
        loadChildren: './store/master-data-store.module#MasterDataStoreModule'
    },
    {
        path: 'materials',
        loadChildren: './material/master-data-material.module#MasterDataMaterialModule'
    },
    {
        path: 'material-groups',
        loadChildren: './material-group/master-data-material-group.module#MasterDataMaterialGroupModule'
    },
    {
        path: 'product-groups',
        loadChildren: './product-group/master-data-product-group.module#MasterDataProductGroupModule'
    },
    {
        path: 'suppliers',
        loadChildren: './supplier/master-data-supplier.module#MasterDataSupplierModule'
    },
    {
        path: 'employees',
        loadChildren: './employee/master-data-employee.module#MasterDataEmployeeModule'
    },
    {
        path: 'inventories',
        loadChildren: './inventory/master-data-inventory.module#MasterDataInventoryModule'
    }
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    exports: [],
    providers: [],
})
export class MasterDataNewModule { }