import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';
import { MaterialGroup } from 'app/models';
import { MaterialGroupService } from 'app/services';

@Component({
	selector: 'app-material-group-details',
	templateUrl: './material-group-details.component.html',
	styleUrls: ['./material-group-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class MaterialGroupDetailsComponent extends PageDetailsComponent<MaterialGroup> {

	form: FormGroup;
	materialGroup: MaterialGroup;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private materialGroupService: MaterialGroupService) {
		super(formBuilder, route, appUtil, materialGroupService);

		this.materialGroup = this.resolvedData as MaterialGroup;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): MaterialGroup {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.materialGroup.id],
			name: [this.materialGroup.name, Validators.required],
			description: [this.materialGroup.description],
			rowVersionString: [this.materialGroup.rowVersionString]
		});
	}
}
