﻿import { MaterialGroupService } from '../../../services/material-group.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialGroupListComponent } from './material-group-list/material-group-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { MaterialGroupDetailsComponent } from './material-group-details/material-group-details.component';
import { MaterialGroupDetailsResolver } from './material-group-details/material-group-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: MaterialGroupListComponent
    },
    {
        path: ':id',
        component: MaterialGroupDetailsComponent,
        resolve: { pageData: MaterialGroupDetailsResolver }
    }
];

@NgModule({
    declarations: [
        MaterialGroupListComponent,
        MaterialGroupDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        MaterialGroupService,
        MaterialGroupDetailsResolver
    ],
})
export class MasterDataMaterialGroupModule {

}