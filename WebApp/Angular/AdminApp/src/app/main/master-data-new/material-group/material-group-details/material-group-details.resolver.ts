import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { MaterialGroupService } from 'app/services/index';
import { MaterialGroup } from 'app/models';
import { Observable, of } from 'rxjs';

@Injectable()
export class MaterialGroupDetailsResolver implements Resolve<MaterialGroup> {
    constructor(private materialGroupService: MaterialGroupService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<MaterialGroup> {
        let id = Number(route.paramMap.get('id'));
        if (id === 0) {
            return of(new MaterialGroup());
        }

        return this.materialGroupService.getById(Number(id));
    }
}
