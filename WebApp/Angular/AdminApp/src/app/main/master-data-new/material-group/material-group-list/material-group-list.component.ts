import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { MaterialGroupService } from 'app/services';
import { MaterialGroup } from 'app/models';

@Component({
	selector: 'app-material-group-list',
	templateUrl: './material-group-list.component.html',
	styleUrls: ['./material-group-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class MaterialGroupListComponent extends PageListComponent<MaterialGroup> {
	displayedColumns = ['select', 'id', 'name', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected materialGroupService: MaterialGroupService) {
		super(route, appUtil, materialGroupService);
	}
}
