import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { EmployeeService } from 'app/services/index';
import { Observable, of } from 'rxjs';
import { Employee } from 'app/models';

@Injectable()
export class EmployeeDetailsResolver implements Resolve<Employee> {
    constructor(private employeeService: EmployeeService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Employee> {
        let id = Number(route.paramMap.get('id'));
        if (id === 0) {
            return of(new Employee());
        }

        return this.employeeService.getById(Number(id));
    }
}