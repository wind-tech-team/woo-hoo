import { EmployeeService } from '../../../../services/employee.service'
import { Employee } from 'app/models/employee.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';

@Component({
	selector: 'app-employee-details',
	templateUrl: './employee-details.component.html',
	styleUrls: ['./employee-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class EmployeeDetailsComponent extends PageDetailsComponent<Employee> {

	form: FormGroup;
	employee: Employee;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private employeeService: EmployeeService) {
		super(formBuilder, route, appUtil, employeeService);
		
		this.employee = this.resolvedData as Employee;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): Employee {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.employee.id],
			name: [this.employee.name, Validators.required],
			phone: [this.employee.phone],
			address: [this.employee.address],
			dateOfBirth: [this.employee.dateOfBirth],
			rowVersionString: [this.employee.rowVersionString]
		});
	}
}
