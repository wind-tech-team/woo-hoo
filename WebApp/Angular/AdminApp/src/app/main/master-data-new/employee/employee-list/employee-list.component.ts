import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { EmployeeService } from 'app/services/employee.service';
import { Employee } from 'app/models';

@Component({
	selector: 'app-employee-list',
	templateUrl: './employee-list.component.html',
	styleUrls: ['./employee-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class EmployeeListComponent extends PageListComponent<Employee> {
	displayedColumns = ['id', 'name', 'phone','address','dateOfBirth', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected employeeService: EmployeeService) {
		super(route, appUtil, employeeService);
	}
}
