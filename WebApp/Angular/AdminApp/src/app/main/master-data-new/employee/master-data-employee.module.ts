import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule,
 MatDatepickerModule, MatNativeDateModule , MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, 
 MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, 
 MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { EmployeeService } from '../../../services/employee.service';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeDetailsResolver } from './employee-details/employee-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: EmployeeListComponent
    },
    {
        path: ':id',
        component: EmployeeDetailsComponent,
        resolve: { pageData: EmployeeDetailsResolver }
    }
];

@NgModule({
    declarations: [
        EmployeeListComponent,
        EmployeeDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        TranslateModule,
        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        EmployeeService,
        EmployeeDetailsResolver
    ],
})
export class MasterDataEmployeeModule {

}