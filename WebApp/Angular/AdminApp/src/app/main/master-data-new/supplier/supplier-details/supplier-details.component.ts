import { SupplierService } from './../../../../services/supplier.service';
import { Supplier } from 'app/models/supplier.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';

@Component({
	selector: 'app-supplier-details',
	templateUrl: './supplier-details.component.html',
	styleUrls: ['./supplier-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class SupplierDetailsComponent extends PageDetailsComponent<Supplier> {

	form: FormGroup;
	supplier: Supplier;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private supplierService: SupplierService) {
		super(formBuilder, route, appUtil, supplierService);

		this.supplier = this.resolvedData as Supplier;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): Supplier {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.supplier.id],
			name: [this.supplier.name, Validators.required],
			description: [this.supplier.description],
			rowVersionString: [this.supplier.rowVersionString]
		});
	}
}
