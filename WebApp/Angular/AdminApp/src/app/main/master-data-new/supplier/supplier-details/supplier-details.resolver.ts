import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { SupplierService } from 'app/services/index';
import { Supplier } from 'app/models';
import { Observable, of } from 'rxjs';

@Injectable()
export class SupplierDetailsResolver implements Resolve<Supplier> {
    constructor(private supplierService: SupplierService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Supplier> {
        let id = Number(route.paramMap.get('id'));
        if (id === 0) {
            return of(new Supplier());
        }

        return this.supplierService.getById(Number(id));
    }
}
