import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { SupplierService } from 'app/services';
import { Supplier } from 'app/models';

@Component({
	selector: 'app-supplier-list',
	templateUrl: './supplier-list.component.html',
	styleUrls: ['./supplier-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class SupplierListComponent extends PageListComponent<Supplier> {
	displayedColumns = ['select', 'id', 'name', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected supplierService: SupplierService) {
		super(route, appUtil, supplierService);
	}
}
