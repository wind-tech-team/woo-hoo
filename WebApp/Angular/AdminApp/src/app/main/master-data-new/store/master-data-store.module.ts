﻿import { StoreService } from '../../../services/store.service';
import { InventoryService } from '../../../services/inventory.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreListComponent } from './store-list/store-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { StoreDetailsComponent } from './store-details/store-details.component';
import { StoreDetailsResolver } from './store-details/store-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: StoreListComponent
    },
    {
        path: ':id',
        component: StoreDetailsComponent,
        resolve: { pageData: StoreDetailsResolver }
    }
];

@NgModule({
    declarations: [
        StoreListComponent,
        StoreDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        StoreService,
        StoreDetailsResolver,
        InventoryService
    ],
})
export class MasterDataStoreModule {

}