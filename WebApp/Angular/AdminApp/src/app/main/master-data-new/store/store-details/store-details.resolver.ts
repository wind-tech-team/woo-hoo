import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { StoreService, InventoryService } from 'app/services/index';
import { Store } from 'app/models';

@Injectable()
export class StoreDetailsResolver implements Resolve<Store> {
    constructor(private storeService: StoreService, private inventoryService: InventoryService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
        const inventories = await this.inventoryService.getDropdownList().toPromise();

        let store = new Store();
        let id = Number(route.paramMap.get('id'));
        if (id !== 0) {
            store = await this.storeService.getById(Number(id)).toPromise();
        }

        return { store, dropdowns: { inventories } };
    }
}
