import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { StoreService } from 'app/services/store.service';
import { Store } from 'app/models';

@Component({
	selector: 'app-store-list',
	templateUrl: './store-list.component.html',
	styleUrls: ['./store-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class StoreListComponent extends PageListComponent<Store> {
	displayedColumns = ['select', 'id', 'name', 'hotline', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected storeService: StoreService) {
		super(route, appUtil, storeService);
	}
}
