import { StoreService } from './../../../../services/store.service';
import { Store } from 'app/models/store.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';

@Component({
	selector: 'app-store-details',
	templateUrl: './store-details.component.html',
	styleUrls: ['./store-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class StoreDetailsComponent extends PageDetailsComponent<Store> {

	form: FormGroup;
	store: Store;
	dropdowns: any;

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private storeService: StoreService) {
		super(formBuilder, route, appUtil, storeService);

		this.store = this.resolvedData.store as Store;
		this.dropdowns = this.resolvedData.dropdowns;
		this.form = this.buildForm();
	}

	protected getDataForSubmit(): Store {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.store.id],
			name: [this.store.name, Validators.required],
			hotline: [this.store.hotline],
			address: this.formBuilder.group({
				street: [this.store.address.street, Validators.required],
				city: [this.store.address.city, Validators.required],
				ward: [this.store.address.ward],
				district: [this.store.address.district, Validators.required],
				country: [this.store.address.country, Validators.required]
			}),
			inventoryId: [this.store.id ? this.store.inventoryId : 0, Validators.required],
			rowVersionString: [this.store.rowVersionString]
		});
	}
}
