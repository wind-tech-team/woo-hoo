import { AppUtil } from 'app/main/_base';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';

import { PageListComponent } from '../../../_base/index';
import { MaterialService } from 'app/services';
import { Material } from 'app/models';

@Component({
	selector: 'app-material-list',
	templateUrl: './material-list.component.html',
	styleUrls: ['./material-list.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})

export class MaterialListComponent extends PageListComponent<Material> {
	displayedColumns = ['select', 'id', 'name', 'materialGroupName', 'inventoryUnit', 'saleUnit', 'averageCostPerSaleUnit',
		'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'actions'];

	constructor(protected route: ActivatedRoute, protected appUtil: AppUtil, protected materialService: MaterialService) {
		super(route, appUtil, materialService);
	}
}
