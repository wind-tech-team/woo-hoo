import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { MaterialService, MaterialGroupService } from 'app/services/index';
import { Material } from 'app/models';

@Injectable()
export class MaterialDetailsResolver implements Resolve<Material> {
    constructor(private materialService: MaterialService, private materialGroupService: MaterialGroupService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
        const materialGroups = await this.materialGroupService.getDropdownList().toPromise();

        let material = new Material();
        let id = Number(route.paramMap.get('id'));
        if (id !== 0) {
            material = await this.materialService.getById(Number(id)).toPromise();
        }

        return { material, dropdowns: { materialGroups } };
    }
}
