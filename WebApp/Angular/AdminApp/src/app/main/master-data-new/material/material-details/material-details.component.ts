import { MaterialService } from './../../../../services/material.service';
import { Material } from 'app/models/material.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PageDetailsComponent, AppUtil } from 'app/main/_base';
import { merge } from 'rxjs';
import { baseDirectiveCreate } from '@angular/core/src/render3/instructions';

@Component({
	selector: 'app-material-details',
	templateUrl: './material-details.component.html',
	styleUrls: ['./material-details.component.scss'],
	animations: fuseAnimations,
	encapsulation: ViewEncapsulation.None
})
export class MaterialDetailsComponent extends PageDetailsComponent<Material> {

	form: FormGroup;
	material: Material;
	dropdowns: any;

	get translateObj(): any {
		return {
			inventoryUnit: this.form.get('inventoryUnit').value ? this.form.get('inventoryUnit').value : this.appUtil.translate.instant('MATERIAL_DETAILS.INVENTORY_UNIT_FIELD_ABBR'),
			saleUnit: this.form.get('saleUnit').value ? this.form.get('saleUnit').value : this.appUtil.translate.instant('MATERIAL_DETAILS.SALE_UNIT_FIELD_ABBR')
		};
	}

	constructor(protected formBuilder: FormBuilder, protected route: ActivatedRoute, protected appUtil: AppUtil, private materialService: MaterialService) {
		super(formBuilder, route, appUtil, materialService);

		this.material = this.resolvedData.material as Material;
		this.dropdowns = this.resolvedData.dropdowns;
		this.form = this.buildForm();
	}

	ngOnInit() {
		super.ngOnInit();

		this.continuousCalculateAverageCostPerSaleUnit();
	}

	protected getDataForSubmit(): Material {
		return this.form.getRawValue();
	}

	private buildForm(): FormGroup {
		return this.formBuilder.group({
			id: [this.material.id],
			name: [this.material.name, Validators.required],
			description: [this.material.description],
			minimumInStock: [this.material.minimumInStock, [Validators.required, Validators.min(0)]],
			inventoryUnit: [this.material.inventoryUnit, Validators.required],
			saleUnit: [this.material.saleUnit, Validators.required],
			inventoryToSaleAmountPerUnit: [this.material.inventoryToSaleAmountPerUnit, [Validators.required, Validators.min(0.0000000000001)]],
			averagePricePerInventoryUnit: [this.material.averagePricePerInventoryUnit, [Validators.required, Validators.min(0.0000000000001)]],
			averageCostPerSaleUnit: [this.material.averageCostPerSaleUnit],
			materialGroupId: [this.material.materialGroupId],
			rowVersionString: [this.material.rowVersionString]
		});
	}

	private continuousCalculateAverageCostPerSaleUnit(): void {
		merge(
			this.form.get('inventoryToSaleAmountPerUnit').valueChanges,
			this.form.get('averagePricePerInventoryUnit').valueChanges
		).subscribe(next => {
			const inventoryToSaleAmountPerUnit = this.form.get('inventoryToSaleAmountPerUnit').value;
			const averagePricePerInventoryUnit = this.form.get('averagePricePerInventoryUnit').value;
			const averageCostPerSaleUnit = this.materialService.calculateAverageCostPerSaleUnit(averagePricePerInventoryUnit, inventoryToSaleAmountPerUnit);
			this.form.get('averageCostPerSaleUnit').setValue(averageCostPerSaleUnit);
		});
	}
}
