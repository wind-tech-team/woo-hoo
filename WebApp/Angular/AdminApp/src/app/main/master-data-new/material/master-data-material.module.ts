﻿import { MaterialService, MaterialGroupService } from '../../../services';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialListComponent } from './material-list/material-list.component';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSelectModule, MatToolbarModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { MaterialDetailsComponent } from './material-details/material-details.component';
import { MaterialDetailsResolver } from './material-details/material-details.resolver';

const routes: Routes = [
    {
        path: '',
        component: MaterialListComponent
    },
    {
        path: ':id',
        component: MaterialDetailsComponent,
        resolve: { pageData: MaterialDetailsResolver }
    }
];

@NgModule({
    declarations: [
        MaterialListComponent,
        MaterialDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,

        TranslateModule,

        FuseSharedModule,
        FuseSidebarModule
    ],
    exports: [],
    providers: [
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500 }
        },
        MaterialService,
        MaterialGroupService,
        MaterialDetailsResolver
    ],
})
export class MasterDataMaterialModule {

}