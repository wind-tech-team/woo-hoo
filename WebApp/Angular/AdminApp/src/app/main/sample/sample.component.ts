//import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as vietnam } from './i18n/vi';
import { from } from 'rxjs';
import { Address as AddressModel } from 'app/models/address.model';

@Component({
    selector: 'sample',
    templateUrl: './sample.component.html',
    styleUrls: ['./sample.component.scss']
})
export class SampleComponent {
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */

    addressModel: AddressModel;
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, vietnam);
        this.addressModel = new AddressModel();
    }

    onAddressModelChanged(newAddress: AddressModel) {
        this.addressModel = newAddress;
        console.log(this.addressModel);
    }
}
