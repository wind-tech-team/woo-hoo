import { CrudModel } from './_base/crud.model';

export class Inventory extends CrudModel {
    name: string;
    storeName: string;
    storeId: number;
    address: string;
}

export class InventoryMaterialInputModel {
    inventoryId: number;
    receiptDate: Date;
    price: number;
    note: string;
    items: InventoryMaterialInputItemModel[] = [];

    constructor(inventoryId: number, receiptDate: Date, price: number, note: string) {
        this.inventoryId = inventoryId;
        this.receiptDate = receiptDate;
        this.price = price;
        this.note = note;
    }
}

export class InventoryMaterialInputItemModel {
    supplierId: number;
    consignmentCode: string;
    materialId: number;
    quantity: number;

    constructor(materialId: number, quantity: number, supplierId: number, consignmentCode: string) {
        this.supplierId = supplierId;
        this.consignmentCode = consignmentCode;
        this.materialId = materialId;
        this.quantity = quantity;
    }
}