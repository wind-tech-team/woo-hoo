import { CrudModel } from "./_base/crud.model";

export class ProductGroup extends CrudModel {
    name: string;
    description: string;
}