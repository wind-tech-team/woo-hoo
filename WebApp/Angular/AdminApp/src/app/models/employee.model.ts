import { CrudModel } from './_base/crud.model';

export class Employee extends CrudModel {
    name: string;
    phone: string;
    address: string;
    dateOfBirth:Date;
}