import { CrudModel } from "./_base/crud.model";

export class Material extends CrudModel {
    code: string;
    name: string;
    description: string;
    minimumInStock: number = 0;
    inventoryUnit: string;
    saleUnit: string;
    inventoryToSaleAmountPerUnit = 0;
    averagePricePerInventoryUnit = 0;
    averageCostPerSaleUnit: number;
    materialGroupId: number;
    materialGroupName: string;
}
