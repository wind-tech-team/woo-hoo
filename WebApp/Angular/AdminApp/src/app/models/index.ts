export class PagedList<T> {
    rowsCount: number;
    data: T[];
}

export class ListRequest {
    searchTerm: string = '';
    sortData: string = '';
    pageNumber: number = 1;
    pageSize: number = 10;
}

export * from './store.model';
export * from './product-group.model';
export * from './supplier.model';
export * from './material.model';
export * from './material-group.model';
export * from './employee.model';
export * from './inventory.model';