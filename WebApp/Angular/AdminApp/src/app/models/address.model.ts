export class Address {
    street: string;
    district: string;
    state: string;
    country: string;
    city: string;
    zipCode: string;
    ward: string;
    addressLine: string;
}