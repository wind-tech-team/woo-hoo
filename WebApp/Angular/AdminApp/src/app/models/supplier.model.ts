import { CrudModel } from "./_base/crud.model";

export class Supplier extends CrudModel {
    name: string;
    description: string;
}