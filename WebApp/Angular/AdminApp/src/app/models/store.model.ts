import { Address } from './address.model';
import { CrudModel } from './_base/crud.model';

export class Store extends CrudModel {
    name: string;
    hotline: string;
    address: Address = new Address();
    inventoryId: number;
}