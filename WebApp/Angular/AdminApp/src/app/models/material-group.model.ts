import { CrudModel } from "./_base/crud.model";

export class MaterialGroup extends CrudModel {
    name: string;
    description: string;
}