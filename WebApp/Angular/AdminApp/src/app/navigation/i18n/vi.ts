export const locale = {
  lang: 'vi',
  data: {
    'NAV': {
      'APPLICATIONS': 'Programlar',
      'SAMPLE': {
        'TITLE': 'Örnek',
        'BADGE': '15'
      },
      'MASTER-DATA': {
        'TITLE': 'Danh Mục',
        'PRODUCT-GROUP': 'Nhóm Sản Phẩm',
        'PRODUCT': 'Sản Phẩm',
        'STORE': 'Cửa Hàng',
        'MATERIAL-GROUP': 'Nhóm nguyên liệu',
        'MATERIAL': 'Nguyên liệu',
        'SUPPLIER': 'Nhà cung cấp',
        'EMPLOYEE': 'Nhân viên',
        'INVENTORY': 'Kho'
      },
      'INVENTORY-MANAGEMENT': {
        'TITLE': 'Quản lý kho',
        'MATERIAL': 'Nguyên liệu',
        'MATERIAL-INPUT': 'Nhập kho',
        'MATERIAL-OUTPUT': 'Xuất kho',
        'PRODUCT': 'Sản phẩm',
        'PRODUCT-INPUT': 'Nhập kho',
        'PRODUCT-OUTPUT': 'Xuất kho'
      }
    }
  }
};
