import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'inevntory',
    title: 'Inventory',
    translate: 'NAV.INVENTORY-MANAGEMENT.TITLE',
    type: 'group',
    children: [
      {
        id: 'inventory-material',
        title: 'Inventory Materials',
        translate: 'NAV.INVENTORY-MANAGEMENT.MATERIAL',
        type: 'collapsable',
        icon: 'view_agenda',
        children: [
          {
            id: 'material-input',
            title: 'Material Input',
            translate: 'NAV.INVENTORY-MANAGEMENT.MATERIAL-INPUT',
            type: 'item',
            url: '/inventory/materials/input'
          },
          {
            id: 'material-output',
            title: 'Material Output',
            translate: 'NAV.INVENTORY-MANAGEMENT.MATERIAL-OUTPUT',
            type: 'item',
            url: '/inventory/materials/output'
          }
        ]
      },
      {
        id: 'inventory-product',
        title: 'Inventory Products',
        translate: 'NAV.INVENTORY-MANAGEMENT.PRODUCT',
        type: 'collapsable',
        icon: 'view_module',
        children: [
          {
            id: 'product-input',
            title: 'Product Input',
            translate: 'NAV.INVENTORY-MANAGEMENT.PRODUCT-INPUT',
            type: 'item',
            url: '/inventory/products/input'
          },
          {
            id: 'product-output',
            title: 'Product Output',
            translate: 'NAV.INVENTORY-MANAGEMENT.PRODUCT-OUTPUT',
            type: 'item',
            url: '/inventory/products/output'
          }
        ]
      }
    ]
  },
  {
    id: 'master-data',
    title: 'Master Data',
    translate: 'NAV.MASTER-DATA.TITLE',
    type: 'group',
    children: [
      {
        id: 'product',
        title: 'Products',
        translate: 'NAV.MASTER-DATA.PRODUCT',
        type: 'collapsable',
        icon: 'view_module',
        children: [
          {
            id: 'product',
            title: 'Products',
            translate: 'NAV.MASTER-DATA.PRODUCT',
            type: 'item',
            url: '/master-data/products'
          },
          {
            id: 'product-group',
            title: 'Product Group',
            translate: 'NAV.MASTER-DATA.PRODUCT-GROUP',
            type: 'item',
            url: '/master-data/product-groups'
          }
        ]
      },
      {
        id: 'material',
        title: 'Materials',
        translate: 'NAV.MASTER-DATA.MATERIAL',
        type: 'collapsable',
        icon: 'view_agenda',
        children: [
          {
            id: 'material',
            title: 'Materials',
            translate: 'NAV.MASTER-DATA.MATERIAL',
            type: 'item',
            url: '/master-data/materials'
          },
          {
            id: 'material-group',
            title: 'Material Group',
            translate: 'NAV.MASTER-DATA.MATERIAL-GROUP',
            type: 'item',
            url: '/master-data/material-groups'
          }
        ]
      },
      {
        id: 'store',
        title: 'Stores',
        translate: 'NAV.MASTER-DATA.STORE',
        type: 'item',
        icon: 'store',
        url: '/master-data/stores'
      },
      {
        id: 'supplier',
        title: 'Suppliers',
        translate: 'NAV.MASTER-DATA.SUPPLIER',
        type: 'item',
        icon: 'local_grocery_store',
        url: '/master-data/suppliers'
      },
      {
        id: 'employee',
        title: 'Employees',
        translate: 'NAV.MASTER-DATA.EMPLOYEE',
        type: 'item',
        icon: 'local_grocery_store',
        url: '/master-data/employees'
      },
      {
        id: 'inventory',
        title: 'Inventory',
        translate: 'NAV.MASTER-DATA.INVENTORY',
        type: 'item',
        icon: 'store',
        url: '/master-data/inventories'
      }
    ]
  },
  {
    id: 'applications',
    title: 'Applications',
    translate: 'NAV.APPLICATIONS',
    type: 'group',
    children: [
      {
        id: 'sample',
        title: 'Sample',
        translate: 'NAV.SAMPLE.TITLE',
        type: 'item',
        icon: 'email',
        url: '/sample',
        badge: {
          title: '25',
          translate: 'NAV.SAMPLE.BADGE',
          bg: '#F44336',
          fg: '#FFFFFF'
        }
      }
    ]
  }
];
