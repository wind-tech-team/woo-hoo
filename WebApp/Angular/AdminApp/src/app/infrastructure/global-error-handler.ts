import { ErrorHandler, Injectable, Injector } from '@angular/core';

import { DialogService } from '../shared/dialog/index';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) { }

  handleError(error) {
    let dialogService = this.injector.get(DialogService);
    let message = error.message ? error.message : error.toString();
    dialogService.showAlert('Error', message);

    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }
}
