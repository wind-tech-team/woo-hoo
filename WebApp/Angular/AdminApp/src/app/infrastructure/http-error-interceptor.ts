import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { DialogService } from "app/shared/dialog";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private dialogService: DialogService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let message = error.message ? error.message : error.toString();
                    this.dialogService.showAlert('Error', message);

                    return throwError(error);
                })
            )
    }
}