﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooHoo.Domain.Helpers
{
    public static class CodeGenerator
    {
        public static string GenerateProductCode()
        {
            return $"SP{DateTime.Now:ddMMyyssff}";
        }

        public static string GenerateMaterialCode()
        {
            return $"NL{DateTime.Now:ddMMyyssff}";
        }

        public static string GenerateOrderCode()
        {
            return $"DH{DateTime.Now:ddMMyyssff}";
        }

        public static string GenerateGoodsReceiptCode()
        {
            return $"PN{DateTime.Now:ddMMyyssff}";
        }
    }
}
