﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.StoreAggregate
{
    public interface IStoreRepository : IRepository<Store>
    {
    }
}
