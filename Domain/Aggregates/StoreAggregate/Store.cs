﻿using System;
using WooHoo.Domain.Seedwork;
using WooHoo.Domain.ValueObjects;

namespace WooHoo.Domain.Aggregates.StoreAggregate
{
    public class Store : AggregateRoot, ISoftDeletable
    {
        public string Name { get; private set; }

        public Address Address { get; private set; }

        public string Hotline { get; private set; }

        private Store() { }

        public Store(string name, string hotline, Address address) : this()
        {
            Set(name, hotline, address);
        }

        public void Set(string name, string hotline, Address address)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            if (address == null)
                throw new ArgumentNullException(nameof(address));

            Name = name;
            Address = address;
            Hotline = hotline;
        }
    }
}
