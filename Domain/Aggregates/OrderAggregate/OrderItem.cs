﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public class OrderItem : Entity
    {
        public int ProductId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
