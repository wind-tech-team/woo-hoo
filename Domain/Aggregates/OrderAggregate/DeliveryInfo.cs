﻿using System.Collections.Generic;
using WooHoo.Domain.Seedwork;
using WooHoo.Domain.ValueObjects;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public class DeliveryInfo : ValueObject
    {
        public string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public Address Address { get; set; }

        private DeliveryInfo() { }

        public DeliveryInfo(string customerName, string phoneNumber, Address address)
        {
            CustomerName = customerName;
            PhoneNumber = phoneNumber;
            Address = address;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return CustomerName;
            yield return PhoneNumber;
            yield return Address;
        }
    }
}
