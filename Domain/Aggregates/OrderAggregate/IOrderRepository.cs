﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
