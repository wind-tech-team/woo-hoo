﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public class OrderType : Enumeration
    {
        public static OrderType Store = new OrderType(1, "Tại Quán");
        public static OrderType Online = new OrderType(2, "Online");

        public OrderType(int id, string name) : base(id, name)
        {

        }
    }
}
