﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public class OrderStatus : Enumeration
    {
        public static OrderStatus New = new OrderStatus(0, "Mới");
        public static OrderStatus InProgress = new OrderStatus(1, "Đang Làm");
        public static OrderStatus Done = new OrderStatus(2, "Hoàn Thành");
        public static OrderStatus Cancelled = new OrderStatus(3, "Hủy");

        public OrderStatus(int id, string name) : base(id, name)
        {

        }
    }
}
