﻿using System;
using System.Collections.Generic;
using WooHoo.Domain.Helpers;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.OrderAggregate
{
    public class Order : AggregateRoot
    {
        public string Code { get; private set; }

        public DeliveryInfo DeliveryInfo { get; private set; }

        public string Note { get; private set; }

        private int _typeId;
        public OrderType Type { get; private set; }

        private int _statusId;
        public OrderStatus Status { get; private set; }

        public int? PromotionId { get; private set; }

        public int StoreId { get; private set; }

        public decimal Price { get; private set; }

        private readonly List<OrderItem> _items;
        public IReadOnlyCollection<OrderItem> Items => _items;

        private Order()
        {
            _items = new List<OrderItem>();
        }

        public Order(DeliveryInfo deliveryInfo, OrderType type, int storeId) : this()
        {
            if (storeId == 0)
                throw new ArgumentOutOfRangeException(nameof(storeId));

            Set(deliveryInfo, type);
        }

        public void Set(DeliveryInfo deliveryInfo, OrderType type)
        {
            DeliveryInfo = deliveryInfo;
            Type = type;
            _typeId = type.Id;
        }

        public void ApplyPromotion(int promotionId, decimal price)
        {

        }
    }
}
