﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.MaterialAggregate
{
    public interface IMaterialRepository : IRepository<Material>
    {
    }
}
