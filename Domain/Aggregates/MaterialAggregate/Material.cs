﻿using System;
using WooHoo.Domain.Helpers;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.MaterialAggregate
{
    public class Material : AggregateRoot, ISoftDeletable
    {
        public string Code { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        /// <summary>
        /// The minimum value in inventory before being warned
        /// </summary>
        public decimal MinimumInStock { get; private set; }

        /// <summary>
        /// The unit in inventory, ex: 10kg apple
        /// </summary>
        public string InventoryUnit { get; private set; }

        /// <summary>
        /// The unit used for selling, ex: 500ml apple juice
        /// </summary>
        public string SaleUnit { get; private set; }

        /// <summary>
        /// Converted amount from Inventory Unit to Sale Unit, ex: 1kg apple = 500ml apple juice
        /// </summary>
        public decimal InventoryToSaleAmountPerUnit { get; private set; }

        /// <summary>
        /// Ex: 1kg apple = 100,000 VND
        /// </summary>
        public decimal AveragePricePerInventoryUnit { get; private set; }

        /// <summary>
        /// This is calculated based on the converted amount and the price per Inventory Unit, ex: 1ml apple juice = 200 VND
        /// </summary>
        public decimal AverageCostPerSaleUnit { get; private set; }

        public int? MaterialGroupId { get; private set; }

        private Material()
        {
        }

        public Material(string name, string inventoryUnit, string saleUnit, decimal inventoryToSaleAmountPerUnit, decimal averagePricePerInventoryUnit,
                        string description = null, decimal minimumInStock = 0) : this()
        {
            Set(name, inventoryUnit, saleUnit, inventoryToSaleAmountPerUnit, averagePricePerInventoryUnit, description, minimumInStock);
        }

        public void AssignToMaterialGroup(int materialGroupId)
        {
            MaterialGroupId = materialGroupId;
        }

        public void Set(string name, string inventoryUnit, string saleUnit, decimal inventoryToSaleAmountPerUnit, decimal averagePricePerInventoryUnit,
            string description = null, decimal minimumInStock = 0)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrEmpty(saleUnit))
                throw new ArgumentNullException(nameof(saleUnit));

            if (string.IsNullOrEmpty(inventoryUnit))
                throw new ArgumentNullException(nameof(inventoryUnit));

            if (inventoryToSaleAmountPerUnit <= 0)
                throw new ArgumentOutOfRangeException(nameof(inventoryToSaleAmountPerUnit));

            if (averagePricePerInventoryUnit <= 0)
                throw new ArgumentOutOfRangeException(nameof(averagePricePerInventoryUnit));

            Name = name;
            InventoryUnit = inventoryUnit;
            SaleUnit = saleUnit;
            InventoryToSaleAmountPerUnit = inventoryToSaleAmountPerUnit;
            AveragePricePerInventoryUnit = averagePricePerInventoryUnit;
            AverageCostPerSaleUnit = AveragePricePerInventoryUnit / InventoryToSaleAmountPerUnit;
            Description = description;
            MinimumInStock = minimumInStock;
        }
    }
}
