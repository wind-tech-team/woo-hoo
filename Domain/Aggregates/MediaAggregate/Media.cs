﻿using System;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.MediaAggregate
{
    public class Media : AggregateRoot
    {
        public string Caption { get; private set; }

        private int _typeId;

        public MediaType Type { get; private set; }

        public string Source { get; private set; }

        public int? ProductId { get; private set; }

        public int? StoreId { get; private set; }

        private Media()
        {

        }

        public Media(MediaType type, string source, string caption = null) : this()
        {
            Set(type, source, caption);
        }

        public void AssignToProduct(int productId)
        {
            ProductId = productId;
        }

        public void AssignToStore(int storeId)
        {
            StoreId = storeId;
        }

        public void Set(MediaType type, string source, string caption = null)
        {
            if (string.IsNullOrEmpty(source))
                throw new ArgumentNullException(nameof(source));

            if (type == null)
                throw new ArgumentNullException(nameof(type));

            Source = source;
            Type = type;
            _typeId = type.Id;
            Caption = caption;
        }
    }
}
