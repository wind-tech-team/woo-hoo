﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.MediaAggregate
{
    public class MediaType : Enumeration
    {
        public static MediaType Photo = new MediaType(1, "Mới");
        public static MediaType Video = new MediaType(2, "Đang Làm");

        public MediaType(int id, string name) : base(id, name)
        {

        }
    }
}
