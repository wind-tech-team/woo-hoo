﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.MaterialGroupAggregate
{
    public interface IMaterialGroupRepository : IRepository<MaterialGroup>
    {
    }
}
