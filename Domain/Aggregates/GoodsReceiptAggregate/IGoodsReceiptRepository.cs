﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.GoodsReceiptAggregate
{
    public interface IGoodsReceiptRepository : IRepository<GoodsReceipt>
    {
    }
}
