﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WooHoo.Domain.Helpers;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.GoodsReceiptAggregate
{
    public class GoodsReceipt : AggregateRoot, ISoftDeletable
    {
        public DateTime ReceiptDate { get; private set; }

        public int InventoryId { get; private set; }

        public decimal Price { get; private set; }

        public string Code { get; private set; }

        public string Note { get; private set; }

        private readonly List<GoodsReceiptItem> _items;
        public IReadOnlyCollection<GoodsReceiptItem> Items => _items;

        private GoodsReceipt()
        {
            _items = new List<GoodsReceiptItem>();
        }

        public GoodsReceipt(DateTime receiptDate, int inventoryId, decimal price, string note = null) : this()
        {
           Set(receiptDate, inventoryId, price, note);
        }

        public void Set(DateTime receiptDate, int inventoryId, decimal price, string note = null)
        {
            if (inventoryId == 0)
                throw new ArgumentOutOfRangeException(nameof(inventoryId));

            ReceiptDate = receiptDate;
            InventoryId = inventoryId;
            Price = price;
            Note = note;
        }

        public void AddItem(GoodsReceiptItem item)
        {
            _items.Add(item);
        }

        public void RemoveItem(GoodsReceiptItem item)
        {
            _items.Remove(item);
        }

        public void UpdateItem(int id, GoodsReceiptItem item)
        {
            var itemToUpdate = _items.SingleOrDefault(x => x.Id == id);
            itemToUpdate?.Set(item.MaterialId, item.Quantity);
        }
    }
}
