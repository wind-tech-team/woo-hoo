﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.GoodsReceiptAggregate
{
    public class GoodsReceiptItem : Entity
    {
        public int MaterialId { get; private set; }

        public decimal Quantity { get; private set; }

        public int? SupplierId { get; private set; }

        public string ConsignmentCode { get; private set; }

        public GoodsReceiptItem(int materialId, decimal quantity, int? supplierId = null, string consignmentCode = null)
        {
            Set(materialId, quantity, supplierId, consignmentCode);
        }

        public void Set(int materialId, decimal quantity, int? supplierId = null, string consignmentCode = null)
        {
            if (materialId == 0)
                throw new ArgumentOutOfRangeException(nameof(materialId));

            if (quantity == 0)
                throw new ArgumentOutOfRangeException(nameof(quantity));

            MaterialId = materialId;
            Quantity = quantity;
            SupplierId = supplierId;
            ConsignmentCode = consignmentCode;
        }
    }
}
