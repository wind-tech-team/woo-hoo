﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.GoodsIssueAggregate
{
    public class GoodsIssue : AggregateRoot, ISoftDeletable
    {
        public DateTime IssueDate { get; private set; }

        public int InventoryId { get; private set; }

        public string Code { get; private set; }

        public string Note { get; private set; }

        private readonly List<GoodsIssueItem> _items;
        public IReadOnlyCollection<GoodsIssueItem> Items => _items;

        private GoodsIssue()
        {
            _items = new List<GoodsIssueItem>();
        }

        public GoodsIssue(DateTime issueDate, int inventoryId, string note = null) : this()
        {
            Set(issueDate, inventoryId, note);
        }

        public void Set(DateTime issueDate, int inventoryId, string note = null)
        {
            if (inventoryId == 0)
                throw new ArgumentOutOfRangeException(nameof(inventoryId));

            IssueDate = issueDate;
            InventoryId = inventoryId;
            Note = note;
        }

        public void AddItem(GoodsIssueItem item)
        {
            _items.Add(item);
        }
    }
}
