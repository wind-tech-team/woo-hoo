﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.GoodsIssueAggregate
{
    public class GoodsIssueItem : Entity
    {
        public int MaterialId { get; private set; }

        public decimal Quantity { get; private set; }

        public int? SupplierId { get; private set; }

        public string ConsignmentCode { get; private set; }

        public DateTime ReceiptDate { get; private set; }

        public GoodsIssueItem(int materialId, decimal quantity, DateTime receiptDate, int? supplierId = null, string consignmentCode = null)
        {
            Set(materialId, quantity, receiptDate, supplierId, consignmentCode);
        }

        public void Set(int materialId, decimal quantity, DateTime receiptDate, int? supplierId = null, string consignmentCode = null)
        {
            if (materialId == 0)
                throw new ArgumentOutOfRangeException(nameof(materialId));

            if (quantity == 0)
                throw new ArgumentOutOfRangeException(nameof(quantity));

            MaterialId = materialId;
            Quantity = quantity;
            SupplierId = supplierId;
            ReceiptDate = receiptDate;
            ConsignmentCode = consignmentCode;
        }
    }
}
