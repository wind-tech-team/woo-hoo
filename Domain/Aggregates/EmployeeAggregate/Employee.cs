﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.EmployeeAggregate
{
    public class Employee : AggregateRoot, ISoftDeletable
    {
        public string Name { get; private set; }

        public string Phone { get; private set; }
        public string Address { get; private set; }

        public DateTime? DateOfBirth { get; private set; }

        private Employee() { }

        public Employee(string name, string phone, string address, DateTime? dateOfBirth) : this()
        {
            Set(name, phone,address, dateOfBirth);
        }

        public void Set(string name, string phone, string address, DateTime? dateOfBirth)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            this.Name = name;
            this.Phone = phone;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
        }
    }
}
