﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.EmployeeAggregate
{
    public interface IEmployeeReposiory : IRepository<Employee>
    {
    }
}
