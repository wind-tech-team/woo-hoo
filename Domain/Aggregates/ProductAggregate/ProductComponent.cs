﻿using System;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.ProductAggregate
{
    public class ProductComponent : Entity
    {
        public int MaterialId { get; private set; }

        public decimal Quantity { get; private set; }

        public string Note { get; private set; }

        private ProductComponent()
        {

        }

        public ProductComponent(int materialId, decimal quantity) : this()
        {
            if (materialId == 0)
                throw new ArgumentNullException(nameof(materialId));

            if (quantity == 0)
                throw new ArgumentOutOfRangeException(nameof(quantity));

            MaterialId = materialId;
            Quantity = quantity;
        }

        public void SetQuantity(decimal quantity)
        {
            Quantity = quantity;
        }
    }
}
