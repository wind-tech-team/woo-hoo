﻿using System;
using System.Collections.Generic;
using System.Linq;
using WooHoo.Domain.Helpers;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.ProductAggregate
{
    public class Product : AggregateRoot, ISoftDeletable
    {
        public string Code { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public decimal Price { get; private set; }

        public bool IsNew { get; private set; }

        public bool IsOutOfOrder { get; private set; }

        public int? ProductGroupId { get; private set; }

        private readonly List<ProductComponent> _components;
        public IReadOnlyCollection<ProductComponent> Components => _components;

        private Product()
        {
            _components = new List<ProductComponent>();
        }

        public Product(string name, decimal price, string description = null, bool isNew = false, bool isOutOfOrder = false) : this()
        {
            Set(name, price, description, isNew, isOutOfOrder);
        }

        public void Set(string name, decimal price, string description = null, bool isNew = false, bool isOutOfOrder = false)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            if (price == 0)
                throw new ArgumentOutOfRangeException(nameof(price));

            Name = name;
            Price = price;
            Description = description;
            IsNew = isNew;
            IsOutOfOrder = isOutOfOrder;
        }

        public void SetNew(bool isNew)
        {
            IsNew = isNew;
        }

        public void SetOutOfOrder(bool isOutOfOrder)
        {
            IsOutOfOrder = isOutOfOrder;
        }

        public void AddComponent(ProductComponent component)
        {
            _components.Add(component);
        }

        public void RemoveComponent(int componentId)
        {
            var componentToRemove = _components.SingleOrDefault(x => x.Id == componentId);
            if (componentToRemove != null)
            {
                _components.Remove(componentToRemove);
            }
        }

        public void UpdateComponent(ProductComponent component)
        {
            var componentToUpdate = _components.SingleOrDefault(x => x.Id == component.Id);
            componentToUpdate?.SetQuantity(component.Quantity);
        }
    }
}
