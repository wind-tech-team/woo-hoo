﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.InventoryAggregate
{
    public class MaterialInventoryItem : Entity
    {
        public int MaterialId { get; private set; }

        public decimal Quantity { get; private set; }

        public DateTime ReceiptDate { get; private set; }

        public int? SupplierId { get; private set; }

        public string ConsignmentCode { get; private set; }

        private MaterialInventoryItem()
        {

        }

        public MaterialInventoryItem(int materialId, decimal quantity, DateTime receiptDate, int? supplierId = null, string consignmentCode = null) : this()
        {
            if (materialId == 0)
                throw new ArgumentOutOfRangeException(nameof(materialId));

            if (quantity == 0)
                throw new ArgumentOutOfRangeException(nameof(quantity));

            MaterialId = materialId;
            Quantity = quantity;
            ReceiptDate = receiptDate;
            SupplierId = supplierId;
            ConsignmentCode = consignmentCode;
        }

        public void Deduct(decimal deductQuantity)
        {
            if (Quantity < deductQuantity)
            {
                throw new InvalidInventoryDeductQuantityException();
            }

            Quantity -= deductQuantity;
        }
    }
}
