﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.InventoryAggregate
{
    public class ProductInventoryItem : Entity
    {
        public int ProductId { get; private set; }

        public decimal Quantity { get; private set; }

        public DateTime ReceiptDate { get; private set; }

        public int? SupplierId { get; private set; }

        public string ConsignmentCode { get; private set; }

        private ProductInventoryItem()
        {

        }

        public ProductInventoryItem(int productId, decimal quantity, DateTime receiptDate, int? supplierId = null, string consignmentCode = null) : this()
        {
            if (productId == 0)
                throw new ArgumentOutOfRangeException(nameof(productId));

            if (quantity == 0)
                throw new ArgumentOutOfRangeException(nameof(quantity));

            ProductId = productId;
            Quantity = quantity;
            ReceiptDate = receiptDate;
            SupplierId = supplierId;
            ConsignmentCode = consignmentCode;
        }
    }
}
