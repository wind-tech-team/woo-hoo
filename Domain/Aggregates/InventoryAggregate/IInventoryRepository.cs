﻿using System.Threading.Tasks;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.InventoryAggregate
{
    public interface IInventoryRepository : IRepository<Inventory>
    {
        Task<Inventory> GetInventoryByStoreIdAsync(int storeID);
    }
}
