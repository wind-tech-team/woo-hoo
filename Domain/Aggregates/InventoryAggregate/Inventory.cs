﻿using System;
using System.Collections.Generic;
using System.Linq;
using WooHoo.Domain.Exceptions;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.InventoryAggregate
{
    public class Inventory : AggregateRoot, ISoftDeletable
    {
        public string Name { get; private set; }

        public string Address { get; private set; }

        public int? StoreId { get; private set; }

        private readonly List<MaterialInventoryItem> _materialItems;
        public IReadOnlyCollection<MaterialInventoryItem> MaterialItems => _materialItems;

        private readonly List<ProductInventoryItem> _productItems;
        public IReadOnlyCollection<ProductInventoryItem> ProductItems => _productItems;

        private Inventory()
        {
            _materialItems = new List<MaterialInventoryItem>();
            _productItems = new List<ProductInventoryItem>();
        }

        public Inventory(string name, string address) : this()
        {
            Set(name, address);
        }

        public void Set(string name, string address)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
            Address = address;
        }

        public void AssignToStore(int storeId)
        {
            StoreId = storeId;
        }

        public void RemoveFromStore()
        {
            StoreId = null;
        }

        public void InputMaterials(List<MaterialInventoryItem> items)
        {
            _materialItems.AddRange(items);
        }

        public void InputProduct(ProductInventoryItem item)
        {
            _productItems.Add(item);
        }

        /// <summary>
        /// Output material items from inventory and return the list of deducted items
        /// </summary>
        public IEnumerable<MaterialInventoryItem> OutputMaterials(IEnumerable<InventoryOutputRequest> itemsToOutput)
        {
            var deductedItemsInInventory = new List<MaterialInventoryItem>();
            foreach (var itemToOutput in itemsToOutput)
            {
                var itemInInventory = _materialItems.SingleOrDefault(x => x.Id == itemToOutput.ItemId);
                if (itemInInventory == null)
                {
                    throw new RecordNotFoundException(nameof(MaterialInventoryItem));
                }

                // deduct items in inventory, if nothing left then remove the item out of inventory
                itemInInventory.Deduct(itemToOutput.Quantity);
                if (itemInInventory.Quantity == 0)
                {
                    _materialItems.Remove(itemInInventory);
                }

                deductedItemsInInventory.Add(itemInInventory);
            }

            return deductedItemsInInventory;
        }

        public class InventoryOutputRequest
        {
            public int ItemId { get; }

            public decimal Quantity { get; }

            public InventoryOutputRequest(int itemId, decimal quantity)
            {
                ItemId = itemId;
                Quantity = quantity;
            }
        }
    }
}
