﻿using System;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.SupplierAggregate
{
    public class Supplier : AggregateRoot, ISoftDeletable
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public Supplier(string name, string description)
        {
            Set(name, description);
        }

        public void Set(string name, string description)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
            Description = description;
        }
    }
}
