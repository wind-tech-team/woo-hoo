﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.SupplierAggregate
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
    }
}
