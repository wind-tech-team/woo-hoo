﻿using System;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.PromotionAggregate
{
    public class Promotion : AggregateRoot, ISoftDeletable
    {
        public string Title { get; private set; }

        public string Description { get; private set; }

        public int? StoreId { get; set; }

        private Promotion()
        {

        }

        public Promotion(string title, string description = null) : this()
        {
            Set(title, description);
        }

        public void Set(string title, string description = null)
        {
            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException(nameof(title));

            Title = title;
            Description = description;
        }

        public void AssignToStore(int storeId)
        {
            StoreId = storeId;
        }
    }
}
