﻿using System;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.ProductGroupAggregate
{
    public class ProductGroup : AggregateRoot
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        private ProductGroup()
        {

        }

        public ProductGroup(string name, string description = null) : this()
        {
            Set(name, description);
        }

        public void Set(string name, string description = null)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
            Description = description;
        }
    }
}
