﻿using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.Aggregates.ProductGroupAggregate
{
    public interface IProductGroupRepository : IRepository<ProductGroup>
    {
    }
}
