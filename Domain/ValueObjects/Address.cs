﻿using System;
using System.Collections.Generic;
using System.Text;
using WooHoo.Domain.Seedwork;

namespace WooHoo.Domain.ValueObjects
{
    public class Address : ValueObject
    {
        public string Street { get; private set; }
        public string District { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Country { get; private set; }
        public string ZipCode { get; private set; }
        public string Ward { get; private set; }
        public string AddressLine { get; private set; }

        private Address() { }

        public Address(string street, string district, string city, string state, string country, string zipcode, string ward, string addressLine)
        {
            Street = street;
            District = district;
            City = city;
            State = state;
            Country = country;
            ZipCode = zipcode;
            Ward = ward;
            AddressLine = addressLine;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return Street;
            yield return District;
            yield return City;
            yield return State;
            yield return Country;
            yield return ZipCode;
            yield return AddressLine;
        }
    }
}
