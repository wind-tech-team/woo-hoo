﻿using System;
using System.Collections.Generic;
using MediatR;
using WooHoo.Domain.Aggregates.InventoryAggregate;

namespace WooHoo.Domain.Events.InventoryEvents
{
    public class InventoryMaterialInputEvent : INotification
    {
        public DateTime ReceiptDate { get; }

        public int InventoryId { get; }

        public decimal Price { get; }

        public string Note { get; }

        public IReadOnlyCollection<MaterialInventoryItem> Items { get; }

        public InventoryMaterialInputEvent(DateTime receiptDate, int inventoryId, decimal price, string note, IReadOnlyCollection<MaterialInventoryItem> items)
        {
            ReceiptDate = receiptDate;
            InventoryId = inventoryId;
            Price = price;
            Note = note;
            Items = items;
        }
    }
}
