﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MediatR;
using WooHoo.Domain.Aggregates.InventoryAggregate;

namespace WooHoo.Domain.Events.InventoryEvents
{
    public class InventoryMaterialOutputEvent : INotification
    {
        public DateTime IssueDate { get; }

        public int InventoryId { get; }

        public string Note { get; }

        /// <summary>
        /// List of inventory items and their quantity to output
        /// </summary>
        public IReadOnlyCollection<Inventory.InventoryOutputRequest> ItemsToOutput { get; }

        /// <summary>
        /// The deducted inventory result after output
        /// </summary>
        public IReadOnlyCollection<MaterialInventoryItem> DeductedItemsInInventory { get; }

        public InventoryMaterialOutputEvent(DateTime issueDate, int inventoryId, string note, IReadOnlyCollection<Inventory.InventoryOutputRequest> itemsToOutput, IReadOnlyCollection<MaterialInventoryItem> deductedItemsInInventory)
        {
            IssueDate = issueDate;
            InventoryId = inventoryId;
            Note = note;
            ItemsToOutput = itemsToOutput;
            DeductedItemsInInventory = deductedItemsInInventory;
        }
    }
}
