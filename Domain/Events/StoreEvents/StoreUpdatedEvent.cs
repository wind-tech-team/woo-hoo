﻿using MediatR;

namespace WooHoo.Domain.Events.StoreEvents
{
    public class StoreUpdatedEvent : INotification
    {
        public StoreUpdatedEvent(int storeId, int inventoryId)
        {
            StoreId = storeId;
            InventoryId = inventoryId;
        }

        public int StoreId { get; }
        public int InventoryId { get; }
    }
}
