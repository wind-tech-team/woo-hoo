﻿using MediatR;

namespace WooHoo.Domain.Events.StoreEvents
{
    public class StoreCreatedEvent : INotification
    {
        public StoreCreatedEvent(int storeId, string storeName, int inventoryId)
        {
            StoreId = storeId;
            StoreName = storeName;
            InventoryId = inventoryId;
        }

        public int StoreId { get; }
        public string StoreName { get; }
        public int InventoryId { get; }
    }
}
