﻿using System;

namespace WooHoo.Domain.Exceptions
{
    public class DomainException : Exception
    {
        public bool IsDbConcurrencyUpdate { get; set; }

        public DomainException()
        { }

        public DomainException(string message)
            : base(message)
        { }

        public DomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
