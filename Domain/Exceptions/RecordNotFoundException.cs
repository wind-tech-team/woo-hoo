﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooHoo.Domain.Exceptions
{
    public class RecordNotFoundException : DomainException
    {
        public string RecordType { get; set; }

        public RecordNotFoundException(string recordType)
        {
            RecordType = recordType;
        }
    }
}
