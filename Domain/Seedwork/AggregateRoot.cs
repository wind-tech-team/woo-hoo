﻿namespace WooHoo.Domain.Seedwork
{
    public abstract class AggregateRoot : Entity
    {
        public byte[] RowVersion { get; set; }
    }
}
