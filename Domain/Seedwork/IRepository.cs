﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WooHoo.Domain.Seedwork
{
    public interface IRepository<T> where T : AggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        Task<T> GetByIdAsync(int id);

        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> filter);

        Task InsertAsync(T entity);

        Task InsertRangeAsync(IEnumerable<T> entities);

        void Update(T entity, string currentRowVersion);

        void Update(T entity, byte[] currentRowVersion);

        void Delete(T entity, string currentRowVersion);
    }
}
