﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace WooHoo.Domain.Seedwork
{
    public interface IUnitOfWork : IDisposable
    {
        Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None, CancellationToken cancellationToken = default(CancellationToken));

        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();
    }
}
