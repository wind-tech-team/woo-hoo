﻿using System;

namespace WooHoo.Domain.Seedwork
{
    public interface IAuditable
    {
        DateTime CreatedDate { get; set; }

        DateTime? UpdatedDate { get; set; }

        string CreatedBy { get; set; }

        string UpdatedBy { get; set; }
    }
}
