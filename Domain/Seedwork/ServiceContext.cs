﻿namespace WooHoo.Domain.Seedwork
{
    public class ServiceContext
    {
        public ServicePrincipal Principal { get; } = new ServicePrincipal();
    }

    public class ServicePrincipal
    {
        public string UserId { get; set; }

        public string Username { get; set; }

        public string Role { get; set; }
    }
}
